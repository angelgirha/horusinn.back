<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('servers', function (Blueprint $table) {
            $table->id();
            $table->string('nombre',100)->unique();
            $table->longText('ip');
            $table->longText('puerto');
            $table->string('usuario');
            $table->longText('password');
            $table->longText('base');
            $table->longText('prefix_base');
            $table->longText('base_central');
            $table->longText('producto');
            $table->tinyInteger('centraliza')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('servers');
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


require __DIR__ . '/auth/auth.php';
require __DIR__ . '/user/user.php';
require __DIR__ . '/ambientes/ambiente.php';
require __DIR__ . '/tareas/tareas.php';
require __DIR__ . '/server/server.php';
require __DIR__ . '/alertas/alertas.php';
require __DIR__ . '/logs/log.php';
require __DIR__ . '/pdf/pdf.php';
require __DIR__ . '/lectores/lectores.php';
require __DIR__ . '/lectoresCron/lectoresCron.php';

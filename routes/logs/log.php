<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\log',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('log/nuevo', 'LogAmbienteController@crearNuevo');
    Route::post('log/actualizar', 'LogAmbienteController@actualizarCampo');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        
    });

});

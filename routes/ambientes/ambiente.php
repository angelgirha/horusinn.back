<?php

use App\Models\MailAux;
use App\Models\User;
use App\Notifications\SignupAuthorization;
use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Ambientes',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {

    Route::post('ambientes/update', 'AmbienteController@update');
    Route::post('ambientes/getTareasAdicionales', 'AmbienteController@getTareasAdicionales');
    Route::post('ambientes/getUltimosPasos', 'AmbienteController@getUltimosPasos');
    Route::post('ambientes/validacionEmpresa', 'AmbienteController@validacionEmpresa');
    Route::post('ambientes/execProcedure', 'AmbienteController@execProcedure');
    Route::post('ambientes/enviarEmail', 'AmbienteController@enviarEmail');

    Route::post('ambientes/prueba', 'AmbienteController@enviarEmail');

    Route::group([
        'middleware' => 'auth:api',
    ], function() {

        Route::post('ambientes/getTablesName', 'AmbienteController@getTablesName');
        Route::post('ambientes/getFunciones', 'AmbienteController@getFunciones');
        Route::post('ambientes/getEvents', 'AmbienteController@getEvents');
        Route::post('ambientes/getSP', 'AmbienteController@getSP');
        Route::post('ambientes/getTriggers', 'AmbienteController@getTriggers');
        Route::post('ambientes/getViews', 'AmbienteController@getViews');
        Route::post('ambientes/getLastCliente', 'AmbienteController@getLastCliente');
        Route::post('ambientes/validacionEmpleadosLectores', 'AmbienteController@validacionEmpleadosLectores');
        Route::post('ambientes/getAllClientes', 'AmbienteController@getAllClientes');
        Route::post('ambientes/getClienteDatos', 'AmbienteController@getClienteDatos');
        Route::post('ambientes/destruyeAmbiente', 'AmbienteController@destruyeAmbiente');
        Route::post('ambientes/creaBase', 'AmbienteController@creaBase');
        Route::post('ambientes/creaTablas', 'AmbienteController@creaTablas');
        Route::post('ambientes/creaFunciones', 'AmbienteController@creaFunciones');
        Route::post('ambientes/creaEvents', 'AmbienteController@creaEvents');
        Route::post('ambientes/creaSP', 'AmbienteController@creaSP');
        Route::post('ambientes/creaTriggers', 'AmbienteController@creaTriggers');
        Route::post('ambientes/creaViews', 'AmbienteController@creaViews');
        Route::post('ambientes/creaInformacionDefault', 'AmbienteController@creaInformacionDefault');
        Route::post('ambientes/insetNewCliente', 'AmbienteController@insertNewCliente');
        Route::post('ambientes/insertsInicial', 'AmbienteController@insertsInicial');
        Route::post('ambientes/enviarEmailDestroy', 'AmbienteController@enviarEmailDestroy');
        Route::get('ambientes/updateDirectorio', 'AmbienteController@updateDirectorio');
        Route::get('ambientes/getAllClienteDireccion', 'AmbienteController@getclientes_direccion');
    });
});

<?php

use App\Models\MailAux;
use App\Notifications\SignupAuthorization;
use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Auth',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('login', 'AuthController@login');
    Route::post('signup', 'AuthController@signup');
    Route::post('reset/inicia', 'AuthController@beginResetKey');
    Route::get('reset/respuesta', 'AuthController@userResetResponse');
    Route::get('reset/approve', 'AuthController@userResetApproval');
    Route::post('reset/data', 'AuthController@userResetData');
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
    Route::get('signup/activa/{token}', 'AuthController@signupActivateConfirmacion');
    Route::get('signup/approve/{token}', 'AuthController@signupApproval');
    Route::get('prueba', 'AuthController@prueba');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

    });
});

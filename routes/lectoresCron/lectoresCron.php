<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\LectoresCron',
    'middleware' => ['api', 'cors'],
    'prefix' => 'auth'
], function () {
    Route::get('lectoresCron/ejecutaRefresh', 'LectoresCronController@ejecutaRefresh');

    Route::group([
        'middleware' => ['auth:api', 'cors'],
    ], function() {
        Route::get('lectoresCron/index', 'LectoresCronController@index');
        Route::post('lectoresCron/save', 'LectoresCronController@store');
        Route::post('lectoresCron/delete/{name}', 'LectoresCronController@deleteOne');
        Route::post('lectoresCron/deletegeneral', 'LectoresCronController@deletegeneral');
        Route::post('lectoresCron/update', 'LectoresCronController@update');
        Route::post('lectoresCron/getAllHorasCron', 'LectoresCronController@getAllHorasCron');
        Route::post('lectoresCron/updateCronHoras', 'LectoresCronController@updateCronHoras');

    });

});

<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'namespace' => 'App\Http\Controllers\Pdf',
    'middleware' => 'api',
    'prefix' => 'auth'
], function () {
    Route::post('pdf/nuevo', 'PdfController@crearNuevo');

    Route::group([
        'middleware' => 'auth:api'
    ], function() {
        
    });

});

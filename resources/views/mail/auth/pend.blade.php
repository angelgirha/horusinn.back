@component('mail::message')
# @lang('Hola!')
<br>

{{$linea1}}
<br>
<br>
{{$linea2}}
<br>

<div class="container">
    <div class="row" style="width: 100%; box-sizing: border-box; display: flex; justify-content:space-between">
        @if ($activo == 1)
                    <div style="width: 33%;">
            @component('mail::button', ['url' => $actionUrl1])
            {{$acceptTextA}}
            @endcomponent
                    </div>
                    <div style="width: 33%;">
            @component('mail::button', ['url' => $actionUrl2])
            {{$acceptTextU}}
            @endcomponent
                    </div>
                    <div style="width: 33%">
            @component('mail::button', ['url' => $actionUrl3, 'color' => 'error'])
            {{$declineText}}
            @endcomponent
                    </div>
        @else
                    <div style="width: 50%;">
            @component('mail::button', ['url' => $actionUrl2])
            {{$acceptTextU}}
            @endcomponent
                    </div>
                    <div style="width: 50%">
            @component('mail::button', ['url' => $actionUrl3, 'color' => 'error'])
            {{$declineText}}
            @endcomponent
                    </div>
        @endif
    </div>
</div>

<br>

@lang('Saludos'),<br>
{{ config('app.name') }}


@endcomponent


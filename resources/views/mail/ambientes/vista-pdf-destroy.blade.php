<html>

<head>
<meta http-equiv=Content-Type content="text/html; charset=utf-8">
<meta name=Generator content="Microsoft Word 15 (filtered)">
<style>

 /* Font Definitions */
 @font-face
	{font-family:Wingdings;
	panose-1:5 0 0 0 0 0 0 0 0 0;}
@font-face
	{font-family:"MS Mincho";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
@font-face
	{font-family:"Cambria Math";
	panose-1:2 4 5 3 5 4 6 3 2 4;}
@font-face
	{font-family:Calibri;
	panose-1:2 15 5 2 2 2 4 3 2 4;}
@font-face
	{font-family:"Lucida Grande";}
@font-face
	{font-family:"\@MS Mincho";
	panose-1:2 2 6 9 4 2 5 8 3 4;}
 /* Style Definitions */

 body{
	 overflow: visible;
	 height: 11.1in;
 }
 .fondo{
	background-image: url({{base_path().$imagen}});
	background-repeat: no-repeat;
	background-size: 100% 100%;
	width: 8.3in;
	height: 11.7in;
	margin-left: -.47in;
	margin-top: -.95in;
	 margin-bottom: -10in;
 }

 p.MsoNormal, li.MsoNormal, div.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoHeader, li.MsoHeader, div.MsoHeader
	{mso-style-link:"Header Char";
	margin:0in;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
a:link, span.MsoHyperlink
	{color:#0563C1;
	text-decoration:underline;}
p.MsoListParagraph, li.MsoListParagraph, div.MsoListParagraph
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpFirst, li.MsoListParagraphCxSpFirst, div.MsoListParagraphCxSpFirst
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpMiddle, li.MsoListParagraphCxSpMiddle, div.MsoListParagraphCxSpMiddle
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:0in;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
p.MsoListParagraphCxSpLast, li.MsoListParagraphCxSpLast, div.MsoListParagraphCxSpLast
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:.5in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;}
span.HeaderChar
	{mso-style-name:"Header Char";
	mso-style-link:Header;}
.MsoChpDefault
	{font-family:"Calibri",sans-serif;}
.MsoPapDefault
	{margin-bottom:8.0pt;
	line-height:107%;}
 /* Page Definitions */
 @page WordSection1
	{size:8.5in 11.0in;
	margin:70.85pt 85.05pt 70.85pt 85.05pt;}
div.WordSection1
	{
		page:WordSection1;
		width: 6.3in;
		height: 5in;
		margin-left: 1in;
		margin-top: .47in;
	}
 /* List Definitions */
 ol
	{margin-bottom:0in;}
ul
	{margin-bottom:0in;}

</style>

</head>

<body>
<div class=fondo>
<div class=WordSection1 lang=EN-US link="#0563C1" vlink="#954F72" style="word-wrap:break-word;">
<br>
<br>
<br>
<br>
<br>
<br>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><span lang=ES-MX style='font-size:12.0pt;font-family:"Arial",sans-serif'>&nbsp;</span></p>

<p class=MsoNormal style='margin-bottom:0in;line-height:150%;page-break-after:
avoid'><b><span lang=ES-MX style='font-size:12.0pt;line-height:150%;font-family:
"Arial",sans-serif'>&nbsp;</span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=ES style='font-size:18.0pt;'>BIENVENIDO
AL SERVICIO CLOUD </span></b></p>

<p class=MsoNormal align=center style='margin-bottom:0in;text-align:center;
line-height:normal'><b><span lang=ES style='font-size:18.0pt;'>{{$producto}}</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal align=right style='text-align:right'><b><span lang=ES
style='font-weight: lighter;'>{{$fecha}}</span></b></p>

<div style='border-top:solid #5B9BD5 1.0pt;border-left:none;border-bottom:solid #5B9BD5 1.0pt;
border-right:none;padding:10.0pt 0in 1.0pt 0in'>

<p class=MsoNormal style='border:none;padding:0in;padding-bottom:10.0pt;
border-bottom:.5pt dotted #ADCCEA'><b><span lang=ES-MX style='font-weight: lighter;font-size:10.0pt;
line-height:107%;font-family:"Arial",sans-serif'>{{$descEmpresa}}</span></b></p>

{{--<p class=MsoNormal style='border:none;padding:0in;padding-top:10.0pt'><b><span
lang=ES-MX style='font-weight: lighter;'>{{$contacto}}</span></b></p>--}}

</div>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='font-weight: lighter;'>Es un gusto para nuestra
empresa, darle la más cordial bienvenida al servicio de Control de Incidencias
y Asistencias, {{$producto}} a través de Infraestructura Cloud.</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='font-weight: lighter;'>Datos de acceso a la plataforma:
</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoListParagraphCxSpFirst style='text-indent:-.25in'><span lang=ES-MX
style='font-family:Wingdings'>&#8226;<span style='font:7.0pt "Times New Roman"'>&nbsp;
</span></span><b><span lang=ES style='color:#002060'>URL acceso directo: </span></b><span
lang=ES-MX><a href="{{$acceso}}">{{$acceso}}</a></span></p>

{{--<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=ES style='font-family:Wingdings;
color:#002060'>&#8226;<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><b><span
lang=ES style='color:#002060'>Ip Engine: {{$ip_engine}}</span></b></p>--}}

<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=ES style='font-family:Wingdings;
color:#002060'>&#8226;<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><b><span
lang=ES style='color:#002060'>No. De cliente: {{$cliente}}</span></b></p>

{{--<p class=MsoListParagraphCxSpMiddle style='margin-bottom:0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=ES style='font-family:Wingdings;
color:#002060'>&#8226;<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><b><span
lang=ES style='color:#002060'>Usuario administrador: {{$usuario}} </span></b>--}}

{{--<p class=MsoListParagraphCxSpLast style='margin-bottom:0in;text-align:justify;
text-indent:-.25in;line-height:150%'><span lang=ES style='font-family:Wingdings;
color:#002060'>&#8226;<span style='font:7.0pt "Times New Roman"'>&nbsp; </span></span><b><span
lang=ES style='color:#002060'>Contraseña de acceso inicial: {{$password}} </span></b></p>--}}

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
150%'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
150%'><b><span lang=ES style='font-weight: lighter;'>Le entregamos también los datos de
contacto con la Mesa de ayuda para acceder al soporte técnico y funcional: </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
150%'><b><span lang=ES style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%'><b><span lang=ES style='font-weight: lighter;'>Celular: 5562275019 </span></b></p>
<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%'><b><span lang=ES style='font-weight: lighter;'>Fijo: 5568332032 </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
115%'><b><span lang=ES style='font-weight: lighter;'>Horario de atención: 9 am a 18:00
pm</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='font-weight: lighter;'>mesadeayuda@girha.com.mx</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='font-weight: lighter;'>Uno de nuestros consultores
se comunicará con usted para dar inicio al proceso de implementación, sin más
agradecer nuevamente su preferencia. </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>
<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>

<img src="{{base_path().$firma}}" style="width: 1in" alt="">

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='font-weight: lighter;'>Erik Mejía Hernández. </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='font-weight: lighter;'>Director General. </span></b></p>

<p class=MsoNormal style='margin-bottom:0in;text-align:justify;line-height:
normal'><b><span lang=ES-MX style='color:#002060'>&nbsp;</span></b></p>

</div>
</div>
</body>

</html>

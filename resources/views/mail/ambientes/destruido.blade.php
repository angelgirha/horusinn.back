@component('mail::message', ['url' => $ruta, 'image' => $ruta_logo])

# @lang('Hola!')
<br>
<div class="container">
    <div class="row">
        <div class="col-sm">
            {{$linea1}}<p style="font-weight: bold">{{$producto}}</p>
        </div>
    </div>
</div>
{{$linea2}}
<br>
<div class="conteiner">
    <div class="row">
        <ul>
            @isset($linea10)
            <li class="col-sm" style="display: flex; justify-content: space-between"><p style="font-weight: bold">{{$linea11}}</p>&nbsp;<p>{{$acceso}}</p></li>
            <li class="col-sm" style="display: flex; justify-content: space-between"><p style="font-weight: bold">{{$linea10}}</p>&nbsp;<p>{{$ip_engine}}</p></li>
            @endisset
            <li class="col-sm" style="display: flex;"><p style="font-weight: bold">{{$linea3}}</p>&nbsp;<p>{{$DS}}</p></li>
            <li class="col-sm" style="display: flex; justify-content: space-between"><p style="font-weight: bold">{{$linea5}}</p>&nbsp;<p>{{$ambiente}}</p></li>
        </ul>
    </div>
</div>


{{$linea12}}
<br>

{{$linea9}}
<br>

@lang('Gracias por su preferencia.'),<br>
{{ $producto }}


@endcomponent


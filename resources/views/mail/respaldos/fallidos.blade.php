@component('mail::message',['url' => 'https://girha.com/', 'image' => 'https://horusinn.back.girha.com/assets/logotipoRedEmail.png'])
# @lang('Hola!')
<br>
<div class="container">
    <div class="row">
        <div class="col-sm">
            {{$linea1}}
        </div>
    </div>
</div>
{{$linea2}}
<br>
<div class="conteiner">
    <div class="row">
        <ul>
            @if($fallo_A != '')
            <li class="col-sm"><p style="font-weight: bold">{{$linea3}}</p><p>{{$fallo_A}}</p></li>
            @endif
            <br>
            @if($fallo_R != '')
            <li class="col-sm"><p style="font-weight: bold">{{$linea4}}</p><p>{{$fallo_R}}</p></li>
            @endif
        </ul>
    </div>
</div>

<br>
{{$linea5}}
<br>

@lang('Saludos'),<br>
{{ config('app.name') }}


@endcomponent

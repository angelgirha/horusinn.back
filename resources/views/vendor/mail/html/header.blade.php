<tr>
<td class="header">
<a href="{{ $url }}" style="display: inline-block;">
@if (trim($slot) === 'Horus')
{{-- <img src="http://192.168.100.212/Horus/cliente/src/assets/img/logotipoRedEmail.png" class="logo" style="max-width: 130px !important;" alt="Horus Logo"> --}}
{{-- <img src="http://localhost/Horus/cliente/src/assets/img/logotipoRedEmail.png" class="logo" style="max-width: 130px !important;" alt="Horus Logo"> --}}
<img src="{{$image}}" class="logo" alt="Horus Logo">
@else
{{ $slot }}
@endif
</a>
</td>
</tr>

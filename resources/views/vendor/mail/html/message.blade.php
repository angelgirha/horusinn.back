@component('mail::layout')

{{-- Header --}}
@slot('header')
@if(isset($url))
    @component('mail::header', ['url' => $url, 'image' => $image])
@else
    @component('mail::header', ['url' => 'https://girha.com/', 'image' => 'https://horusinn.back.girha.com/assets/logotipoRedEmail.png'])
@endif
{{ config('app.name') }}
@endcomponent
@endslot

{{-- Body --}}
{{ $slot }}

{{-- Subcopy --}}
@isset($subcopy)
@slot('subcopy')
@component('mail::subcopy')
{{ $subcopy }}
@endcomponent
@endslot
@endisset

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
© {{ date('Y') }} {{ config('app.name') }}. @lang('All rights reserved.')
@endcomponent
@endslot
@endcomponent

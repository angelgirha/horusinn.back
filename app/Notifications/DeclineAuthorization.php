<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class DeclineAuthorization extends Notification
{
    use Queueable;

    private $user;
    private $tipo;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $tipo)
    {
        $this->user = $user;
        $this->tipo = $tipo;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        switch ($this->tipo) {
            case 1:
                return (new MailMessage)
                    ->subject('Cuenta Declinada')
                    ->line('Su cuenta ha sido rechazada, para cualquier aclaración por favor comuníquese al área de soporte.');
                break;
            case 2:
                return (new MailMessage)
                    ->subject('Recuperación de acceso declinada')
                    ->line('Su solicitud ha sido rechazada y podra continuar con su antigua llave, para cualquier aclaración por favor comuníquese al área de soporte.');
        }
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

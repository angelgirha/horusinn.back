<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class SignupAuthorization extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        if(!empty($notifiable->acceptA)){
            $acceptA = $notifiable->acceptA;
            $acceptAText = $notifiable->acceptAText;
            $activo = 1;
        }else{
            $acceptA = '';
            $acceptAText = '';
            $activo = 0;
        }

        $opt = [
            'actionUrl1'            => $acceptA,
            'actionUrl2'            => $notifiable->acceptU,
            'actionUrl3'            => $notifiable->decline,
            'correo'                => $notifiable->emailAccount,
            'nombre'                => $notifiable->nombreTo,
            'displayableActionUrl'  => str_replace(['mailto:', 'tel:'], '', 'jsdj'),
            'linea1' => $notifiable->linea1,
            'linea2' => $notifiable->linea2,
            'acceptTextA' => $acceptAText,
            'acceptTextU' => $notifiable->acceptUText,
            'declineText' => $notifiable->declineText,
            'asunto' => $notifiable->asunto,
            'activo'=> $activo,
        ];

        return (new MailMessage)
            ->subject($notifiable->asunto)
            ->markdown('mail.auth.pend',$opt);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ResetForm extends Notification
{
    use Queueable;

    private $user;
    private $url;
    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, $url)
    {
        $this->user = $user;
        $this->url = $url;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $token = base64_encode($this->user->activation_token.'[<>]'.$this->user->email);
        $url = $this->url.'/api/auth/reset/respuesta?token='.$token.'&reset=1';
        return (new MailMessage)
            ->subject('Recupera tu acceso')
            ->line('Se ha aprobado una solicitud de Horus con este correo electrónico, para continuar debes reingresar tus datos y obtener tu nueva llave de acceso (archivo .pem).')
            ->action('Formulario de recuperación', $url);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

<?php

namespace App\Http\Controllers\Ambientes;

use App\Http\Controllers\Controller;
use App\Models\Ambientes;
use App\Models\Server;
use App\Notifications\AmbientCreated;
use App\Notifications\AmbienteDestruido;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Validator;

use function GuzzleHttp\json_decode;

class AmbienteController extends Controller
{

    public function getTablesName(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);
        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getTablesName($request));
    }

    //funcion que sirve para buscar datos de los clientes desde ambiente.php 
    //hasta el ambientecontroler
    public function getclientes_direccion()
    {
        //funcion que encuentra a los clientes y sus datos como son
        //ambiente direccion y más datos
        //crear sentencia para que mande error si no hay conexion

        $respuesta = Ambientes::f_getclientes_direccion();

        //validador de si hay o no datos en el json..
        /*if($respuesta->fails()){
            return response()->json([
                'message' => 'Error al visualizar datos',
                'type' => 'g-401'
            ],401);
        } */
        //var_dump($respuesta[0]); die();
        return json_encode($respuesta);
    }

    public function updateDirectorio(){
        Ambientes::getBaseConnection($defaultDb , "mysql");
        $resp = Ambientes::f_obtenerInfoForDirectorio();

        return Ambientes::f_updateDirectorio($resp, $defaultDb);
    }

    public function getFunciones(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);
        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getFunciones($request));
    }

    public function getEvents(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getEvents($request));
    }

    public function getSP(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }

        return json_encode(Ambientes::f_getSP($request));
    }
    public function getTriggers(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }

        return json_encode(Ambientes::f_getTriggers($request));
    }
    public function getViews(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }

        return json_encode(Ambientes::f_getViews($request));
    }

    public function getTareasAdicionales(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getTareasAdicionales($request));
    }

    public function getUltimosPasos(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'cliente' => 'required',
            'producto' => 'required'
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        $info = Ambientes::f_getUltimosPasos($request->producto);

        if (count($info) != 0) {
            return json_encode($info);
        } else {
            return response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => 'procedure no Encontrado'
            ], 500);
        }
    }

    public function getLastCliente(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'producto' => 'required'
        ]);

        if ($validacion->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getLastCliente($request->toArray(), $request->producto));
    }

    public function getAllClientes(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'producto' => 'required'
        ]);

        if ($validacion->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }

        $respuesta = Ambientes::f_getAllClientes($request->toArray(), $request->producto);
        return response()->json(mb_convert_encoding($respuesta, "UTF-8", "auto"));
    }

    public function getClienteDatos(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'cliente' => 'required'
        ]);

        if ($validacion->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        return json_encode(Ambientes::f_getClienteDatos($request->toArray(), $request->producto));
    }

    public function validacionEmpresa(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'rfc' => 'required|string',
            'descEmpresa' => 'required|string'
        ]);

        if ($validacion->fails()) {
            $errores = '';
            foreach ($validacion->errors()->all() as $mensajes) {
                $errores = $mensajes . ', ' . $errores;
            }
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401',
                'error' => $errores
            ], 400);
        }

        $info = Ambientes::f_validacionEmpresa($request);
        if (is_array($info)) {
            if (count($info) != 0) {
                $respuesta = response()->json([
                    'message' => 'Error en ' . $request->descEmpresa . ' o ' . $request->codigoEmpresa,
                    'type' => 'g-405',
                    'error' => 'La empresa ya se encuentra registrada.'

                ], 400);
            } else {
                $respuesta = response()->json([
                    'message' => 'no existe registro'
                ], 200);
            }
        } else {
            $respuesta = response()->json([
                'message' => 'Error en el Servidor',
                'typo' => 'g-504',
                'error' => $info->getMessage()
            ], 500);
        }

        return $respuesta;
    }

    public function validacionEmpleadosLectores(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'server_name' => 'required|string',
            'server_2' => 'required|string',
            'empleadosActivos' => 'required|string',
            'lectoresLinea' => 'required|string'
        ]);

        if ($validacion->fails()) {
            $errores = '';
            foreach ($validacion->errors()->all() as $mensajes) {
                $errores = $mensajes . ', ' . $errores;
            }
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401',
                'error' => $errores
            ], 400);
        }

        $totalEmpleados = Ambientes::f_getEmpleados($request);
        $totalLectores = Ambientes::f_getLectores($request);

        if (intval($totalEmpleados) <= 0) {
            $respuesta = response()->json([
                'message' => 'El servidor no cuenta con registros'
            ], 200);
        } elseif (intval($totalEmpleados) + intval($request["empleadosActivos"]) >= 10000 && intval($totalLectores) + intval($request["lectoresLinea"]) < 400) {
            $espacioDisponible = 10000-intval($totalEmpleados);
            $respuesta = response()->json([
                'server' => 'mysql',
                'message' => 'El servidor '.$request["server_name"].' no cuenta con el suficiente espacio, espacio disponible para '.$espacioDisponible.' empleados'
            ], 500);
        } elseif (intval($totalLectores) + intval($request["lectoresLinea"]) >= 400 && intval($totalEmpleados) + intval($request["empleadosActivos"]) < 10000) {
            $espacioDisponible = 400-intval($totalLectores);
            $respuesta = response()->json([
                'server' => 'postgresql',
                'message' => 'El servidor '.$request["server_2"].' no cuenta con el suficiente espacio, espacio disponible para '.$espacioDisponible.' lectores'
            ], 500);
        } elseif(intval($totalLectores) + intval($request["lectoresLinea"]) >= 400 && intval($totalEmpleados) + intval($request["empleadosActivos"]) >= 10000){
            $espacioDisponibleL = 400-intval($totalLectores);
            $espacioDisponibleE = 10000-intval($totalEmpleados);
            $respuesta = response()->json([
                'server' => 'postgresql',
                'message' => 'El servidor '.$request["server_2"].' no cuenta con el suficiente espacio, espacio disponible para '.$espacioDisponibleE.' empleados y el servidor '.$request["server_name"].' cuenta con el espacio disponible para '.$espacioDisponibleL.' lectores'
            ], 500);
        } else {
            $respuesta = response()->json([
                'message' => 'Validacion exitosa'
            ], 200);
        }

        return $respuesta;
    }

    public function creaBase(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'server_name'   => 'required|string',
            'cliente'    => 'required',
        ]);
        if ($validacion->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401',
            ], 400);
        }

        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaBase($request->cliente . $request->bd_origen, $request);
        } else {
            $resp = Ambientes::f_creaBase($request->cliente, $request);
        }

        if ($resp['result']) {
            $respuesta = response()->json([
                'name' => $resp['name']
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error al crear la base de datos',
                'type' => 'g-504',
                'error' => $resp['name']
            ], 500);
        }

        return $respuesta;
    }

    public function destruyeAmbiente(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'bd_nueva'   => 'required|string',
            'cliente'    => 'required',
            'producto'    => 'required',

        ]);
        if ($validacion->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401',
            ], 400);
        }

        if ($request->producto == "girha" || $request->producto == "biotime_cloud") {
            Ambientes::getBaseConnection($defaultDb , "mysql");
            if($request->producto == "girha"){
                $serverG = Server::where('servers.producto', 'girha')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
            }else{
                $serverG = Server::where('servers.producto', 'biotime_cloud')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
            }
            if(isset($serverG)){
                Server::decodeServer($serverG,2);
                Ambientes::setBaseConnection($serverG, $serverG->base_central);
                $ips = DB::select('call sp_obtener_ips_clientes(' . $request->cliente . ')');
                $ips = json_decode(json_encode($ips[0]), true);
                $request->ip_psql = $ips['@ip_postgresql_descrip'];
                $request->ip_mysql = $ips['@ip_mysql_descrip'];
                Ambientes::setBaseConnection($defaultDb,$defaultDb->bd_name,"mysql");
                $resp = Ambientes::f_destruyeCLiente($request,$serverG);
            }
        } else {
            $resp = Ambientes::f_destruyeCLiente($request, $request);
        }
        if ($resp['result']) {
            $respuesta = response()->json([
                'name' => $resp['name']
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error al crear la base de datos',
                'type' => 'g-504',
                'error' => $resp['name']
            ], 500);
        }

        return $respuesta;
    }

    public function creaTablas(Request $request)
    {
        $validacion = Validator::make($request->all(), [
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        if ($validacion->fails()) {
            return response()->json(
                [
                    'message' => 'Error en los datos. Favor de verificarlos',
                    'type' => 'g-401',
                ],
                400
            );
        }

        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaTablas($request, $request->cliente . $request->bd_origen);
        } else {
            $resp = Ambientes::f_creaTablas($request, $request->cliente);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'tabla_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }
        return $respuesta;
    }

    public function creaFunciones(Request $request)
    {

        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaFunciones($request->cliente . $request->bd_origen, $request, $request->name);
        } else {
            $resp = Ambientes::f_creaFunciones($request->cliente, $request, $request->name);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }

        return $respuesta;
    }


    public function creaEvents(Request $request)
    {
        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);

        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaEvents($request->cliente . $request->bd_origen, $request, $request->name);
        } else {
            $resp = Ambientes::f_creaEvents($request->cliente, $request, $request->name);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }

        return $respuesta;
    }


    public function creaSP(Request $request)
    {

        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);

        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaSP($request->cliente . $request->bd_origen, $request, $request->name);
        } else {
            $resp = Ambientes::f_creaSP($request->cliente, $request, $request->name);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }

        return $respuesta;
    }


    public function creaTriggers(Request $request)
    {

        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaTriggers($request->cliente . $request->bd_origen, $request, $request->name);
        } else {
            $resp = Ambientes::f_creaTriggers($request->cliente, $request, $request->name);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }

        return $respuesta;
    }


    public function creaViews(Request $request)
    {

        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaViews($request->cliente . $request->bd_origen, $request, $request->name);
        } else {
            if($request->name == 'v_accesos_solicitudes'){
                Ambientes::f_insertInfoClienteDirectorio($request);
            }
            $resp = Ambientes::f_creaViews($request->cliente, $request, $request->name);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp->getMessage()
            ], 500);
        }

        return $respuesta;
    }

    public function creaInformacionDefault(Request $request)
    {
        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        if ($request->producto == "liber" && $request->bd_origen == "_historico") {
            $resp = Ambientes::f_creaInformacionDefault($request->cliente . $request->bd_origen, $request->server_name, $request->name, $request);
        } else {
            $resp = Ambientes::f_creaInformacionDefault($request->cliente, $request->server_name, $request->name, $request);
        }
        if ($resp === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'g-500',
                'error' => $resp->getMessage()
            ], 500);
        }
        return $respuesta;
    }

    public function insertNewCliente(Request $request)
    {
        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        $resp = Ambientes::f_insertNewCliente($request->cliente, $request->server_name, $request->email, $request);
        if ($resp === true) {
            $respuesta = response()->json([
                'message' => 'Cliente registrado'
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $resp
            ], 500);
        }
        return $respuesta;
    }

    public function execProcedure(Request $request)
    {
        $request->validate([
            'server_name' => 'required|string'
        ]);
        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);
        $info = Ambientes::f_execProcedure($request);
        if (is_array($info)) {
            $resp = json_decode(json_encode($info[0]), true);
            if($resp['@codigo'] == 1){
                $respuesta = response()->json([
                    'name' => 'sp executado',
                    'message' => $resp['@msj']
                ], 200);
            } else {
                $respuesta = response()->json([
                    'message' => 'Error en ' . $request->name,
                    'error' => $resp['@msj']
                ], 500);
            }
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'error' => $info->getMessage()
            ], 500);
        }
        return $respuesta;
    }

    public function enviarEmail(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'bd_nueva' => 'required|string',
            'password' => 'required|min:8|string'
        ]);
        $mensaje = response()->json([
            'message' => 'Error en los datos, favor de verificarlos.'
        ], 400);

        $confirmacion = [
            'DS' => $request->descEmpresa,
            'CE' => $request->codigoEmpresa,
            'ambiente' => $request->cliente,
            'usuario' => $request->usuario,
            'pass' => $request->password,
            'contacto' => $request->contacto,
            'fecha' => now(),
            'linea1' => 'Se te informa que se ha completado el proceso de creación de ambiente para la plataforma:',
            'linea2' => 'Los datos de acceso al servicio son: ',
            'linea3' => 'Nombre de la empresa: ',
            'linea4' => 'Clave Empresa: ',
            'linea5' => 'Cliente: ',
            'linea6' => 'Usuario: ',
            'linea7' => 'Contraseña: ',
            'linea8' => 'Fecha Disponibilidad: ',
            'linea9' => 'Ante cualquier duda, favor de comunicarse con soporte.'
        ];

        switch ($request->producto) {
            case "girha":
                $engine = $this->getServerEngine($request->server_2);
                $confirmacion['producto'] = "Girha, control de asistencias.";
                $confirmacion['ip_engine'] = $engine->ip;
                $confirmacion['linea10'] = 'Ip Engine: ';
                $confirmacion['acceso'] = "https://asistencias.girha.com/app/";
                $confirmacion['linea11'] = 'URL acceso directo: ';
                $confirmacion['linea12'] = 'Anexo: encontrará la carta de entrega-bienvenida del director general adjunto a este correo. ';
                $confirmacion['ruta_logo'] = 'https://girha.com/assets/img/logo-dark.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                break;
            case "mi_asistencia":
                $engine = $this->getServerEngine($request->server_2);
                $confirmacion['producto'] = "Mi Asistencia.";
                $confirmacion['ip_engine'] = $engine->ip;
                $confirmacion['acceso'] = "https://miasistencia.girha.com/";
                $confirmacion['linea12'] = "";
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/mi_asistencia.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                $confirmacion['subproducto'] = $request->subproducto;
                break;
            case "liber":
                $confirmacion['producto'] = "Liber.";
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/logotipoRedEmail.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                break;
            case "biotime_cloud":
                $engine = $this->getServerEngine($request->server_2);
                $confirmacion['producto'] = "BioTime Cloud.";
                $confirmacion['ip_engine'] = $engine->ip;
                $confirmacion['linea10'] = 'Ip Engine: ';
                $confirmacion['acceso'] = "https://asistencias2.girha.com/app_zk/";
                $confirmacion['linea11'] = 'URL acceso directo: ';
                $confirmacion['linea12'] = 'Anexo: encontrará la carta de entrega-bienvenida del director general adjunto a este correo. ';
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/BioTimeCloud.png';
                $confirmacion['ruta'] = '';
                break;
        }

        try {
            $correos = explode(';', $request->email);
            foreach ($correos as $correo) {
                Notification::route('mail', $correo)->notify(new AmbientCreated($confirmacion));
            }
            $mensaje = response()->json([
                'message' => 'Correo Enviado. '
            ], 200);
        } catch (Exception $e) {
            $mensaje = response()->json([
                'message' => 'fallo envio',
                'error' => $e->getMessage()
            ], 502);
        }

        return $mensaje;
    }


    public function enviarEmailDestroy(Request $request)
    {
        $request->validate([
            'email' => 'required',
            'bd_nueva' => 'required|string',

        ]);
        $mensaje = response()->json([
            'message' => 'Error en los datos, favor de verificarlos.'
        ], 400);

        $confirmacion = [
            'DS' => $request->descEmpresa,
            'ambiente' => $request->cliente,
            'fecha' => now(),
            'linea1' => 'Se te informa que se ha completado el proceso de destrucción de ambiente para la plataforma:',
            'linea2' => 'Los datos del ambiente eran: ',
            'linea3' => 'Nombre de la empresa: ',
            'linea5' => 'Cliente: ',
            'linea9' => 'Ante cualquier duda, favor de comunicarse con soporte.'
        ];

        switch ($request->producto) {
            case "girha":
                $confirmacion['producto'] = "Girha, control de asistencias.";
                $confirmacion['linea12'] = 'Anexo: encontrará la carta de entrega-bienvenida del director general adjunto a este correo. ';
                $confirmacion['acceso'] = "https://asistencias.girha.com/app/";
                $confirmacion['ruta_logo'] = 'https://girha.com/assets/img/logo-dark.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                break;
            case "mi_asistencia":
                $confirmacion['CE'] = '';
                $confirmacion['producto'] = "Mi Asistencia.";
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/mi_asistencia.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                $confirmacion['acceso'] = "https://miasistencia.girha.com/";
                $confirmacion['linea4'] = '';
                $confirmacion['linea6'] = '';
                $confirmacion['linea7'] = '';
                $confirmacion['linea8'] = '';
                $confirmacion['usuario'] = '';
                $confirmacion['pass'] = '';
                $confirmacion['linea12'] = 'Anexo: encontrará la carta de entrega-bienvenida del director general adjunto a este correo. ';
                break;
            case "liber":
                $confirmacion['producto'] = "Liber.";
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/logotipoRedEmail.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                break;
            case "biotime_cloud":
                $confirmacion['CE'] = '';
                $confirmacion['producto'] = "BioTime Cloud.";
                $confirmacion['linea4'] = '';
                $confirmacion['linea6'] = '';
                $confirmacion['linea7'] = '';
                $confirmacion['linea8'] = '';
                $confirmacion['usuario'] = '';
                $confirmacion['pass'] = '';
                $confirmacion['acceso'] = "https://asistencias2.girha.com/app_zk/";
                $confirmacion['linea12'] = 'Anexo: encontrará la carta de entrega-bienvenida del director general adjunto a este correo. ';
                $confirmacion['ruta_logo'] = 'https://horusinn.back.girha.com/assets/BioTimeCloud.png';
                $confirmacion['ruta'] = 'https://girha.com/';
                break;
        }


            $correos = explode(';', $request->email);
            foreach ($correos as $correo) {
                Notification::route('mail', $correo)
                    ->notify(new AmbienteDestruido($confirmacion));
            }
            $mensaje = response()->json([
                'message' => 'Correo Enviado. '
            ], 200);


        return $mensaje;
    }

    // tocado
    public function insertsInicial(Request $request)
    {
        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
            'name'          => 'required|string'
        ]);

        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);

        $inset = Ambientes::f_insetsLiber($request->cliente, $request->server_name, $request);
        if ($request->producto == 'liber' && $inset === true) {
            $respuesta = response()->json([
                'name' => 'funcion_' . $request->name
            ], 201);
        } else {
            $respuesta = response()->json([
                'message' => 'Error en ' . $request->name,
                'type' => 'SqlError',
                'error' => $inset
            ], 500);
        }

        return $respuesta;
    }

    // public function update(Request $request)
    // {
    //     Config::set('app.key', 'base64:6MWtFEEIzJ4CvpnNZEsh/V624mFeCCfEWpPtMSeLfbg=');
    //     return decrypt($request->info);
    //     Ambientes::updateCer(4, 'liber', $request);

    //     return response()->json([
    //         'name' => 'funcion_'
    //     ], 201);

    //     $request->validate([
    //         'server_name'   => 'required|string',
    //         'cliente'       => 'required',
    //         'name'          => 'required|string'
    //     ]);
    //     $respuesta = response()->json([
    //         'error' => 'Error'
    //     ], 400);

    //     if ($request->producto == 'liber' && Ambientes::f_insetsLiber($request->cliente, $request->server_name, $request)) {
    //         $respuesta = response()->json([
    //             'name' => 'funcion_' . $request->name
    //         ], 201);
    //     }

    //     return $respuesta;
    // }

    private function getServerEngine($nombre)
    {
        $serverE = Server::where('nombre', $nombre)->first();
        Server::decodeServer($serverE);
        return $serverE;
    }
}

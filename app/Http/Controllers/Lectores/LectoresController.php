<?php

namespace App\Http\Controllers\Lectores;

use App\Http\Controllers\Controller;
use App\Models\Bitacora;
use App\Models\Lectores;
use Illuminate\Http\Request;

class LectoresController extends Controller
{
    public function index()
    {
        $clientes = Lectores::f_getClientes();
        return response()->json($clientes);
    }

    public function getLectoresByCliente(Request $request)
    {
        $lectores = Lectores::f_getLectoresByCliente($request->id, $request->ip_psql, $request->producto);
        return $lectores;
    }

    public function getbitacoraBySerie(Request $request){

        if(!isset($request->op)){
            $request->validate([
                'no_serie'   => 'required|string',
            ]);
        }

        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);

        $respuesta = response()->json(Lectores::getBitacoraLectorBySerie($request));
        return $respuesta;
    }

    public function savebitacoraBySerie(Request $request) {
        $request->validate([
            'problema'   => 'required|string',
            'observacion'   => 'required|string',
            'registra'   => 'required|integer',
        ]);

        $respuesta = response()->json([
            'error' => 'Error'
        ], 400);

        $insetBitacora = response()->json(Lectores::saveBitacoraLectorBySerie($request));

        if($insetBitacora){
            $respuesta = response()->json([
                'message' => 'Servidor existente',
                'type' => '200'
            ], 200);
        }else{
            $respuesta = response()->json([
                'message' => 'Error al insertar el registro en la bitacora',
                'type' => '400'
            ], 400);
        }

        return $respuesta;
    }

    public function updateBitacora(Request $request){
        $bitacora = $request->toArray();

        $fallaron = 0;
        foreach ($bitacora as $registro) {
            $val = Bitacora::updateBitacoraByRegistro($registro);
            if (!isset($val)) {
                $fallaron++;
            }
        }
        if ($fallaron > 0) {
            return response()->json([
                'message' => 'Error, favor de contactar a soporte.',
                'code' => 500,
                'error' => 'Error al guardar la información',
            ], 500);
        }

        return response()->json([
            'message' => "Bitacora actualizada",
            'code' => 200
        ], 200);
    }
}

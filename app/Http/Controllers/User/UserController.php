<?php

namespace App\Http\Controllers\User;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function allUsers(Request $request)
    {
        return User::all();
    }

    public function deleteUser(Request $request)
    {
        $request->validate([
            'id'   => 'required',
            'email'    => 'required|string|email',
        ]);

        $user=User::where('id' , $request->id);
        if(!$user){
            return response()->json([
                'error' => true,
                'message'  => "usuario no encontrado",
            ], 404);
        }
        $user->delete();
        $user->forceDelete();
        return response()->json([
            'error' => false,
            'message'  => "Usuario con el id:  # $request->id ha sido eliminado",
        ], 200);
    }


    public function editUser(Request $request)
    {
        $request->validate([
            'id'   => 'required',
            'email'    => 'required|string|email',
            'nombre'    => 'required|string',
        ]);

        $user=User::find( $request->id);
        if(!$user){
            return response()->json([
                'error' => true,
                'message'  => "usuario no encontrado",
            ], 404);
        }
        $user->nombre= $request->nombre;
        $user->email= $request->email;
        $user->save();
        return response()->json([
            'error' => false,
            'message'  => "Usuario con el id:  # $request->id ha sido actualizado",
        ], 200);
    }


}

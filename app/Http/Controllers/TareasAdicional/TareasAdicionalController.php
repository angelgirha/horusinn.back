<?php

namespace App\Http\Controllers\TareasAdicional;

use App\Http\Controllers\Controller;
use App\Models\Server;
use App\Models\Tareas_adicional;
use Illuminate\Http\Request;

class TareasAdicionalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Tareas_adicional  $tareas_adicional
     * @return \Illuminate\Http\Response
     */
    public function show(Tareas_adicional $tareas_adicional)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Tareas_adicional  $tareas_adicional
     * @return \Illuminate\Http\Response
     */
    public function edit(Tareas_adicional $tareas_adicional)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Tareas_adicional  $tareas_adicional
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Tareas_adicional $tareas_adicional)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Tareas_adicional  $tareas_adicional
     * @return \Illuminate\Http\Response
     */
    public function destroy(Tareas_adicional $tareas_adicional)
    {
        //
    }

    public function getTareasByProduct(Request $request)
    {

        $request->validate([
            'server_name'   => 'required|string',
            'cliente'       => 'required',
        ]);
        $respuesta = response()->json([
            'error' => 'Error'], 400);
        $serverInfo=Server::getServerInfo($request->server_name);

        $respuesta = response()->json([
            'respuesta' => 'exito',
            'tareas'=> Tareas_adicional::getTareasByProduct($serverInfo->nombre)], 201,);


        return $respuesta;
    }

}

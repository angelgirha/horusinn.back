<?php

namespace App\Http\Controllers\Alertas;

use App\Console\Commands\Ssh2_crontab_manager;
use App\Console\Commands\Ssh2_phpseclib3_contrab_manager;
use App\Models\Alertas;

use App\Models\Ambientes;
use App\Models\Server;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Notifications\FailsBacks;
use Exception;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use phpseclib3\Crypt\PublicKeyLoader;
use phpseclib3\Net\SSH2;
use function GuzzleHttp\json_decode;

class AlertasController extends Controller
{
    //private $ruta = "/home/alex/Documentos/resp"; // pruebas
    private $ruta = "/media/Disco001/RespaldosGenerales";

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $serves = Alertas::all()->makeHidden(['updated_at', 'created_at'])->toArray();
        echo json_encode($serves);;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        $alertas = $request->toArray();

        foreach ($alertas as $alertaN) {
            $alertaN = json_decode(json_encode($alertaN), true);
            $alertaN = Alertas::createAlert($alertaN);
            $arg_datent_a['actualizar'] = 2;
            $arg_datent_a['cron_job'] = $alertaN->sentencia;
            $arg_datent_a['activar_alertas'] = $alertaN->estado;
            $arg_datent_a['correos_alertas'] = $alertaN->correos;
            $arg_datent_a['tiempo_alertas'] = $alertaN->hora;
            if ($alertaN->estado == 1) {
                $alertaN->sentencia = $this->cronJobAlertaGirhaBd($arg_datent_a);
                $alertaN->save();
            }
        }

        $mensaje = "Datos actualizados correctamente";

        return response()->json($mensaje);
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Models\Alertas $alertas
     * @return \Illuminate\Http\Response
     */
    public function show(Alertas $alertas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Models\Alertas $alertas
     * @return \Illuminate\Http\Response
     */
    public function validarServer()
    {
        $mensaje = null;
        $servers = Server::where('servers.producto', 'girha')->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->where('servers_conexiones.centraliza', 0)->get();

        $serverSSH = $this->getServerSsh($servers);
        if (!$serverSSH) {
            $mensaje = response()->json([
                'message' => 'Servidor no existente',
                'type' => '404'
            ], 404);
        } else {
            $mensaje = response()->json([
                'message' => 'Servidor existente',
                'type' => '200'
            ], 200);
        }
        return $mensaje;
    }

    /**
     * Update the specified resource in storage.
     *
     */
    public function update(Request $request)
    {
        $alertas = $request->toArray();

        foreach ($alertas as $alertaN) {
            $alertaN = json_decode(json_encode($alertaN), true);
            $alertaO = Alertas::find($alertaN['id']);
            Alertas::updateAlert($alertaN);
            $alertaN = Alertas::find($alertaN['id']);
            if ($alertaN->hora != $alertaO->hora || $alertaN->correos != $alertaO->correos) {
                $arg_datent_a['actualizar'] = 1;
            } else {
                $arg_datent_a['actualizar'] = 0;
            }

            $arg_datent_a['cron_job'] = $alertaN->sentencia;
            $arg_datent_a['activar_alertas'] = $alertaN->estado;
            $arg_datent_a['correos_alertas'] = $alertaN->correos;
            $arg_datent_a['tiempo_alertas'] = $alertaN->hora;
            $alertaN->sentencia = $this->cronJobAlertaGirhaBd($arg_datent_a);
            $alertaN->save();
        }

        $mensaje = "Datos actualizados correctamente";

        return response()->json($mensaje, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Models\Alertas $alertas
     * @return \Illuminate\Http\Response
     */
    public function destroy(Alertas $alertas)
    {
        //
    }

    private function cronJobAlertaGirhaBd($arg_datent_a)
    {
        $priv =
            'PuTTY-User-Key-File-2: ssh-rsa
Encryption: none
Comment: jorge.gomez@girha.com.mx
Public-Lines: 6
AAAAB3NzaC1yc2EAAAABJQAAAQEAlpdtQW+azBU5XVK2an9K4Y3l21KYF2cKYPd4
svgC00mCTbUdis57YljLPC75CFxiS+GJr/lHLW3r2eB5ImnjK5o6oDniIr4gvF/n
/zLzD1orLne19kjKalUgtlTkCbacTxLz+bkbW3QsauJLl1jM3mjlZ50E6ONW2rtz
8Q1booKeUk/r1IwlLul+aEBOOnFkt/cV9Om0xA0+bI1XPFXeOAt3y+1A0XwKFec6
h4VNDyEf3IitSMwttgAg/s4AtXYeMe1fQu68BcQm8cicHXmRz4rjgtmvHsQzNIcP
jKd6UK2CoQunnSBZcbMNZMFHkHXSvwubN5FT8AAu8OrN9gQBsQ==
Private-Lines: 14
AAABAH4r2BQ66XqOUqgitIK9rXDeq9Nutsd/2ENnjqPI38XH94024WZ1paVs/U4Z
hIp26pKaXppbO6KaXdI4nNek0xaxpr2YR9frZ4kSFWcc2XuQvGUz3aTVM/g5fEW8
vw8OnqMdt6e2w+TW9MhBDuaWgiIgiNpFSU2ANANeyOyOpriVqjKmNJ+/ToK+usF1
JfmEWVceBo59QeKYjsMjI80WWLAqLW/9oWklwmAdLpLgs6TLoPhiuMvj9c6ouL/9
o4JrpzT/po/wecxzipxtPjO6gOis6nDMove82wh2TvUQpqUJz678GGVVQQG+GuKB
jGCzaY+++mP9gPvlpq32r4oGzZkAAACBAMox5DoYs/P6jPluV1yZiCD+XLZ/LcPi
/ZfwR48qUzi1v6No8SXrk+XdomyQP6gas4/iNL5NaCliCtEUGonBs8iIAfLXnpXt
JOPrEzD4PwS4hlofngwTOL4rdkfaGtBjobB3Udmd1Vi/HbbP7ohvpp2WkjLxb0SF
N4z27OHe62+vAAAAgQC+qily7KmJLPS+2TPU0fB1/wsuWA2qgAb8F37v664rI6Xg
OCg5NtwbMOgrBebfDrRT+yhtQdgQflU5mjInaJyvIFcyAZQ54oj4bQm9zrbJ8MrM
aP0SFWf7/vcvptOg9oQ9YSGP2O2Bj3ih2Q7VhBoaQxmYQmcaoTqpfPBD3bWcnwAA
AIEAx+twtwQp8Z72uiXh54UzBE34eFg56Dzz4gmBmyCEChMwya09gugOrMLnuPI1
IlaI4GFf4aKJHKmwLJ8M2XNrf/aitSOMqIOs1aZC43Lz2v74/iYFqaXXJuoaquOq
MwR4TJTSNIXGHQ7GAMRrRrIpJxXZq0tZy/CSAgZ/curU2lk=
Private-MAC: c35fab1c53d28c1bf22541c31c5ec43996b4e679';

        $crontab = new Ssh2_phpseclib3_contrab_manager('34.70.146.156', '22', 'jorge.gomez', $priv);

        //$crontab = new Ssh2_crontab_manager('localhost', '22', 'zanzakigus', 'Sasukecry1');
        //$crontab = new Ssh2_crontab_manager('localhost', 22, 'alex', 'alex1708*');
        // $crontab = new Ssh2_crontab_manager('localhost', 22, 'gdesarrollo', 'Girha*12345');

        if ($arg_datent_a['actualizar'] == 1 && $arg_datent_a['activar_alertas'] == 1) {
            $crontab->remove_cronjob($arg_datent_a['cron_job']);
        }

        if ($arg_datent_a['activar_alertas'] == 1) {

            $hora = explode(":", date('G:i', strtotime($arg_datent_a['tiempo_alertas'])));
            if (str_split($hora[1])[0] > 0) {
                $minutos = $hora[1];
            } else {
                $minutos = str_split($hora[1])[1];
            }
            $fecha_hora_ejecucion = $minutos . ' ' . $hora[0] . ' * * *';
            $job = $fecha_hora_ejecucion . ' curl --data "correos=' . $arg_datent_a['correos_alertas'] . '" ' . url('/api/auth/alertas/ejecutaAlerta');
            $crontab->append_cronjob($job);
            return $job;
        } else {
            $crontab->remove_cronjob($arg_datent_a['cron_job']);
            return $arg_datent_a['cron_job'];
        }
    }

    public function ejecutaAlerta(Request $request)
    {
        $reporte = null;
        $reportes = array();

        $servers = Server::where('producto', 'girha')->where('centraliza', 0)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->get();
        $serverC = Server::where('producto', 'girha')->where('centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
        Server::decodeServer($serverC);
        $serverSSH = $this->getServerSsh($servers);
        foreach ($servers as $server) {

            if ($server->tipo_conexion == "MySql") {
                $bd = 'mysql';
                switch ($server->producto) {
                    case 'girha':
                        $server->nombre = str_replace("Girha ", "", $server->nombre);
                        break;
                    case 'cloudclock':
                        $server->nombre = str_replace("M.A. ", "", $server->nombre);
                        break;
                    default:
                        $server->nombre = str_replace("M.A. ", "", $server->nombre);
                        break;
                }
                $server->nombre = str_replace(" ", "", $server->nombre);
                $reporte = $this->validarRespaldo($server, $serverSSH, $serverC, $bd);
                array_push($reportes, [
                    "servidor" => $server->nombre,
                    "reporte" => $reporte
                ]);
                if (count($reporte['no_verificados_archivos']) > 0 || count($reporte['no_verificados_registro']) > 0) {
                    $this->correoFallido($request->correos, $reporte, $server->nombre);
                }
            }
        }
    }

    private function getServerSsh($servers)
    {
        $ssh = null;
        foreach ($servers as $server) {
            Server::decodeServer($server);
            if ($server->tipo_conexion == "SSH") {
                $ssh = $server;
            }
        }
        return $ssh;
    }

    private function validarRespaldo($server, $serverSsh, $serverCentral, $bd)
    {
        $verificados_archivos = array();
        $verificados_registro = array();
        $no_verificado_archivo = array();
        $no_verificado_registro = array();

        Ambientes::setBaseConnection($server, $bd);
        $clientes = DB::select("show databases like '%cliente%'");
        if ($serverSsh->tipoPassSsh == "key") {
            $key = PublicKeyLoader::load($serverSsh->password);

            $ssh = new SSH2($serverSsh->ip);
            if (!$ssh->login($serverSsh->usuario, $key)) {
                throw new \Exception('Login failed');
            }
        } else {
            $conexion = ssh2_connect($serverSsh->ip, $serverSsh->puerto);
            ssh2_auth_password($conexion, $serverSsh->usuario, $serverSsh->password);

        }
        $nombre_carpeta = date("Y-m-d_-_", strtotime(now()));
        $nombre_carpeta .= "01-00-01";
        if ($serverSsh->tipoPassSsh == "key") {
            $stream = $ssh->exec('ls -lS "' . $this->ruta . "/" . $server->nombre . "/" . $nombre_carpeta . '"');
        } else {
            $stream = ssh2_exec($conexion, 'ls -lS ' . $this->ruta . "/" . $server->nombre . "/" . $nombre_carpeta);

        }

        if (!$stream) {
            return response()->json(['message' => 'no hay respuesta'], 500);
        } else {
            $serversBack = $this->limpiarClientes($stream, $serverSsh);
            $basenameIndex = 8;
            $tamanoIndex = 4;
            foreach ($clientes as $cliente) {
                $clienteArr = json_decode(json_encode($cliente), true);
                for ($i = 1; $i < count($serversBack); $i++) {
                    if (str_contains($serversBack[$i][$basenameIndex], $clienteArr["Database (%cliente%)"])) {

                        if ((int)$serversBack[$i][$tamanoIndex] > 100) {

                            array_push($verificados_archivos, $clienteArr["Database (%cliente%)"]);
                        } else {
                            array_push($no_verificado_archivo, $clienteArr["Database (%cliente%)"]);
                        }
                    } else {
                        array_push($no_verificado_archivo, $clienteArr["Database (%cliente%)"]);
                    }
                }
                $no_verificados_archivos = $this->filtrarLista($verificados_archivos, $no_verificado_archivo);
                Ambientes::setBaseConnection($serverCentral, 'obelsys');
                $serverbase = DB::select("select bd_respaldada from obelsys.respaldos_bit where bd_respaldada = '" . $clienteArr["Database (%cliente%)"] . "'");;

                if (count($serverbase) == 1) {
                    array_push($verificados_registro, $clienteArr["Database (%cliente%)"]);
                } else {
                    array_push($no_verificado_registro, $clienteArr["Database (%cliente%)"]);
                }

                $no_verificados_registro = $this->filtrarLista($verificados_registro, $no_verificado_registro);
            }
        }
        unset($conexion);
        return [
            'bases totales' => count($clientes),
            'verificados' => count($verificados_archivos),
            'respaldos_totales' => count($serversBack) - 1,
            'respaldos_registrados' => count($verificados_registro),
            'no_verificados_archivos' => $no_verificados_archivos,
            'no_verificados_registro' => $no_verificados_registro
        ];
    }

    private function limpiarClientes($stream, $serverSsh)
    {
        if ($serverSsh->tipoPassSsh = "key") {
            $respuesta = $stream;
        } else {
            stream_set_blocking($stream, true);
            $stream_out = ssh2_fetch_stream($stream, SSH2_STREAM_STDIO);
            $respuesta = stream_get_contents($stream_out);

        }

        $clientes_resp = explode("\n", $respuesta);
        for ($i = 0; $i < count($clientes_resp); $i++) {
            $clientes_resp[$i] = str_replace("  "," ",$clientes_resp[$i]);
            $clientes_resp[$i] = explode(" ", $clientes_resp[$i]);
        }
        unset($clientes_resp[0]);
        return $clientes_resp;
    }

    private function filtrarLista($verificados, $no_verificados)
    {
        $nuevo = array_unique(array_filter($no_verificados, function ($v) use ($verificados) {
            return !in_array($v, $verificados);
        }));


        return $nuevo;
    }

    private function correoFallido($correo, $reporte, $server)
    {
        $reporte_archivos = '';
        $reporte_resgistros = '';
        if (count($reporte['no_verificados_archivos']) > 0) {
            foreach ($reporte['no_verificados_archivos'] as $fallo_archivo) {
                $reporte_archivos .= $fallo_archivo . " ";
            }
        }

        if (count($reporte['no_verificados_registro']) > 0) {
            foreach ($reporte['no_verificados_registro'] as $fallo_archivo) {
                $reporte_resgistros .= $fallo_archivo . " ";
            }
        }

        $reporte = [
            'fecha' => now(),
            'linea1' => 'Reporte diario de verificación de respaldos: ',
            'linea2' => 'El servidor ' . $server . ' presento un fallo al validar los respaldos de las siguintes bases de datos: ',
            'linea3' => 'No se encontraron los archivos de respaldo o el archivo es muy pequeño de: ',
            'fallo_A' => $reporte_archivos,
            'linea4' => 'No se encontraron los registros de respaldo de: ',
            'fallo_R' => $reporte_resgistros,
            'linea5' => 'Ante cualquier duda, favor de comunicarse con soporte.'
        ];

        try {
            Notification::route('mail', $correo)
                ->notify(new FailsBacks($reporte));
            $mensaje = response()->json([
                'message' => 'Correo Enviado. '
            ], 200);
        } catch (Exception $e) {
            $mensaje = response()->json([
                'message' => 'fallo envio',
                "error" => $e->getMessage()
            ], 502);
        }
    }
}

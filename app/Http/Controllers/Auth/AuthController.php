<?php

namespace App\Http\Controllers\Auth;


use App\Models\MailAux;
use App\Models\User;
use App\Http\Controllers\Controller;
use App\Mail\NewUserPend;
use App\Mail\NewUserRequest;
use App\Notifications\ActivationSuccess;
use App\Notifications\DeclineAuthorization;
use App\Notifications\ResetForm;
use App\Notifications\SignupActivate;
use App\Notifications\SignupAuthorization;
use Carbon\Carbon;
use DateTime;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Pikirasa\RSA;

class AuthController extends Controller
{
    //protected const OPENSSH='C:/wamp64/bin/php/php7.4.9/extras/ssl/openssl.cnf'; //para windows
    //protected const URL = 'http://127.0.0.1:4200';
    //protected const URLB = 'http://127.0.0.1:8000';
    protected const URL = 'https://horusinn.girha.com';
    protected const URLB = 'http://horusinn.back.girha.com';
    // protected const URL = 'http://192.168.100.212/Horus/cliente';
    protected const OPENSSH = '/etc/ssl/openssl.cnf'; //para linux_ubuntu
    // protected const OPENSSH = '/etc/pki/tls/openssl.cnf'; //para linux_centos

    public function signup(Request $request)
    {
        //Valida que sean requeridos, el tipo, y unique si es que existen en la tabla usuarios
        $validar = Validator::make($request->all(), [
            'nombre'   => 'required|string',
            'email'    => 'required|string|email',
            'password' => 'required|string',
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }

        $user = User::getIdUser($request->email);
        if (count($user) > 0) {
            return response()->json([
                'message' => 'Error en los datos. Correo ya registrado y activo.',
                'type' => 'g-401'
            ], 401);
        }

        $user = User::getIdUser($request->email, 0);
        if (count($user) > 0) {
            return response()->json([
                'message' => 'Error en los datos. Correo ya registrado y sin activar.',
                'type' => 'g-401'
            ], 401);
        }

        $rsa = new RSA(null, null, null, self::OPENSSH);
        if (!$rsa->create()) {
            return response()->json([
                'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                'type' => 'g-501'
            ], 500);
        }
        $encrypted = $rsa->base64Encrypt($request->password);

        $user = new User([
            'password'    => $encrypted,
            'nombre'      => $request->nombre,
            'email'       => $request->email,
            'status'      => 0,
            'public_key'  => $rsa->getPublicKeyFile(),
            'private_key' => $rsa->getPrivateKeyFile(),
            'activation_token' => $this->temporalLinkToken()
        ]);

        if (!$user->save()) {
            return response()->json([
                'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                'type' => 'g-502'
            ], 500);
        }

        $datos_correo = [
            'emailAccount' => $request->email,
            'nombreTo'      => $request->nombre,
            'email'  => 'soporte.operativo@girha.com.mx',
            'decline' => url('/api/auth/signup/approve/' . base64_encode($user->activation_token . '[<>]2')),
            'acceptA' => url('/api/auth/signup/approve/' . base64_encode($user->activation_token . '[<>]1[<>]1')),
            'acceptU' => url('/api/auth/signup/approve/' . base64_encode($user->activation_token . '[<>]1[<>]2')),
            'linea1' => 'Se realizo el alta de una nueva cuenta con el nombre de ' . $request->nombre . ' y el correo electrónico ' . $request->email . '.',
            'linea2' => 'Para continuar con el proceso es necesario realizar alguna de las siguientes acciones:',
            'acceptAText' => 'Aprobar admin',
            'acceptUText' => 'Aprobar usuario',
            'declineText' => 'Declinar',
            'asunto' => 'Horus petición de aprobación',
        ];

        try {
            Mail::to($datos_correo['email'])->send(new NewUserRequest($datos_correo));
            Mail::to($datos_correo['emailAccount'])->send(new NewUserPend());
            if (count(Mail::failures()) > 0) {
                return response()->json([
                    'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                    'type' => 'g-503a',
                    'error' => Mail::failures()
                ], 502);
            }
            return response()->json([
                'message' => 'Usuario creado'
            ], 201);
        } catch (Exception $e1) {
            return response()->json([
                'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                'type' => 'g-503',
                'error' => Mail::failures()
            ], 503);
        }
    }

    public function login(Request $request)
    {

        $validar = Validator::make($request->all(), [
            'email'       => 'required|string|email|exists:users',
            'password'    => 'required|string|min:8',
            'private_key' => 'required|string',
        ]);

        if ($validar->fails()) {
            return response()->json([
                'message' => 'Datos erroneos. Correo electronico invalido',
                'type' => 'g-401'
            ], 401);
        }

        $user = User::getIdUser($request->email);

        $user = !empty($user) && isset($user[0]->id) ? User::find($user[0]->id) : false ;
        if (!$user) {
            return response()->json([
                'message' => 'Datos erroneos. Favor de verificarlos',
                'type' => 'g-401'
            ], 404);
        }
        $publicKey = $user->public_key;
        $privateKey = $request->private_key;
        try {
            $rsa = new RSA($publicKey, $privateKey, null, self::OPENSSH);
            $decrypted = $rsa->base64Decrypt($user->password);
        } catch (Exception $e) {
            return response()->json([
                'message' => 'Datos erroneos. key invalida',
                'type' => 'g-402'
            ], 403);
        }

        if ($decrypted === $request->password && isset($user)) {
            $tokenResult = $user->createToken('Personal Access Token');
            $token = $tokenResult->token;
            // $token->expires_at = Carbon::now()->addMinutes(30);//oficial
            $token->expires_at = Carbon::now()->addWeek(1); //para pruebas

            $token->save();

            return response()->json([
                'access_token' => $tokenResult->accessToken,
                'token_type'   => 'Bearer',
                'expires_at'   => Carbon::parse($tokenResult->token->expires_at)->toDateTimeString(),
                'profile' => $user->tipo,
                'id' => $user->id
            ], 202);
        }else{
            return response()->json([
                'message' => 'Datos erroneos. Contraseña invalida',
                'type' => 'g-403'
            ], 403);
        }

    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message' =>
        'sesion_cerrada'], 202);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }

    public function signupApproval($token)
    {
        $tokenArray = explode('[<>]', base64_decode($token));
        $user = User::where('activation_token', $tokenArray[0])->first();
        if (!$user) {
            $tipo = $tokenArray[1] == 1 ? 'aprobación' : 'declinación';
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link de ' . $tipo . ' es invalido, favor de comunicarse con el área de soporte para más información.');
            $button = base64_encode(1);
            $buttonText = base64_encode('Inicio');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text . '&button=' . $button . '&buttonText=' . $buttonText);
        }
        if ($tokenArray[1] == 1) {
            $user->status = 1;
            $user->activation_token = $this->temporalLinkToken();
            if ($tokenArray[2] == 1) {
                $user->tipo = 1;
            }
            $user->save();
            $user->notify(new SignupActivate($user, self::URL, 1));
            $titulo = base64_encode('Autorizada');
            $text = base64_encode('La cuenta fue aprobada  exitosamente, se envió un correo al usuario para informar de la actualización de su estado.');
        } else {
            $user->notify(new DeclineAuthorization($user, 1));
            DB::table('users')->where('id', '=', $user->id)->delete();
            $titulo = base64_encode('Declinada');
            $text = base64_encode('La cuenta fue declinada exitosamente, se envió un correo al usuario para informar de la actualización de su status.');
        }
        return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
    }

    public function signupActivate($token)
    {
        $tokenArray = explode('[<>]', base64_decode($token));

        $begin = strtotime($tokenArray[1] . ' +1 day');
        $stop = strtotime('now');
        if ($begin < $stop) {
            $titulo = base64_encode('Vencido');
            $text = base64_encode('El link de activación esta vencido, favor de comunicarse con el área de soporte para más información.');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
        }

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link de activación es invalido, favor de comunicarse con el área de soporte para más información.');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
        }
        $file = "private_key.pem";
        $pem = fopen($file, "w") or die("Error al crear llaves!");
        fwrite($pem, $user->private_key);
        fclose($pem);

        $user->status = 1;
        $user->activation_token = '';
        $user->private_key = '';
        $user->email_verified_at = (new DateTime('now'))->format('Y-m-d H:i:s');
        $user->save();

        header('Content-Description: File Transfer');
        header('Content-Disposition: attachment; filename=' . basename($file));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header('Content-Length: ' . filesize($file));
        header("Content-Type: text/plain");
        readfile($file);
        unlink($file);
        $user->notify(new ActivationSuccess(self::URL));
    }

    public function signupActivateConfirmacion($token){
        $tokenArray = explode('[<>]', base64_decode($token));

        $begin = strtotime($tokenArray[1] . ' +1 day');
        $stop = strtotime('now');
        if ($begin < $stop) {
            $titulo = base64_encode('Vencido');
            $text = base64_encode('El link de activación esta vencido, favor de comunicarse con el área de soporte para más información.');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
        }

        $user = User::where('activation_token', $token)->first();
        if (!$user) {
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link de activación es invalido, favor de comunicarse con el área de soporte para más información.');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
        }

        $titulo = base64_encode('A un paso');
        $text = base64_encode('Para terminar de recuperar tu acceso es necesario descargar la llave, por favor haz clic en el botón de abajo para obtenerla y finalizar.');
        $uri = base64_encode(url('/api/auth/signup/activate/' . $user->activation_token));
        $button = base64_encode(1);
        $buttonText = base64_encode('Obtener archivo pem');
        $url =  self::URL . '/api/auth/signup/activa/'.$user->activation_token;
        $subject = 'Recuperacion de acceso';
        $line = 'Se ha creado y aprobado una recuperacion de acceso de Horus con este correo electrónico, para continuar debes confirmar tu cuenta y obtendrás tu llave de acceso (archivo .pem).';

        return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text . '&uri=' . $uri . '&button=' . $button . '&buttonText=' . $buttonText);
    }

    private function temporalLinkToken()
    {
        return  base64_encode(Str::random(60) . '[<>]' . (new DateTime('now'))->format('Y-m-d H:i:s'));
    }

    public function beginResetKey(Request $request)
    {
        $request->validate([
            'email'       => 'required|string|email',
        ]);
        $user = User::getIdUser($request->email);
        if (!count($user) > 0) {
            $user = User::getIdUser($request->email, 0);
            $id = isset($user[0]['id']) ? $user[0]['id'] : '';
            if($id > 0){
                return response()->json([
                    'message' => 'Su solicitud esta en proceso',
                    'type' => 'g-401'
                ], 401);
            }else{
                return response()->json([
                    'message' => 'Usuario no encontrado',
                    'type' => 'g-404'
                ], 401);
            }
        }
        $user = User::find($user[0]->id);
        $user->activation_token = self::temporalLinkToken();

        $user2 = new MailAux([
            'emailAccount' => '',
            'nombreTo' => $user->nombre,
            'email' => $request->email,
            'decline' => url('/api/auth/reset/respuesta?token=' . base64_encode($user->activation_token . '[<>]2')),
            'acceptU' => url('/api/auth/reset/respuesta?token=' . base64_encode($user->activation_token . '[<>]1[<>]2')),
            'linea1' => $user->nombre . ' se ha solicitado el reseteo de la llave de tu cuenta Horus.',
            'linea2' => 'Para continuar o cancelar con el proceso es necesario realizar alguna de las siguientes acciones:',
            'acceptText' => 'Iniciar proceso',
            'acceptUText' => 'Aprobar',
            'declineText' => 'Declinar',
            'asunto' => 'Solicitud de recuperación de acceso.',


        ]);

        $user2->notify(new SignupAuthorization($user2));
        $user->save();
        return response()->json([
            'message' => 'Usuario notificado'
        ], 201);
    }

    public function userResetResponse(Request $request)
    {
        $tokenArray = explode('[<>]', base64_decode($request->token));
        $user = User::where('activation_token', $tokenArray[0])->first();
        if (!$user) {
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link es invalido, favor de comunicarse con el área de soporte para más información.');
            $button = base64_encode(1);
            $buttonText = base64_encode('Inicio');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text . '&button=' . $button . '&buttonText=' . $buttonText);
        }
        if ($tokenArray[1] == 1) {
            $user->activation_token = $this->temporalLinkToken();
            $user->status = 0;
            $user->save();
            $datos_correo = [
                'emailAccount' => $user->email,
                'nombreTo'      => $user->nombre,
                'email'  => 'soporte.operativo@girha.com.mx',
                'decline' => url('/api/auth/reset/approve?token=' . base64_encode($user->activation_token . '[<>]2')),
                'acceptA' => url('/api/auth/reset/approve?token=' . base64_encode($user->activation_token . '[<>]1[<>]1')),
                'acceptU' => url('/api/auth/reset/approve?token=' . base64_encode($user->activation_token . '[<>]1[<>]2')),
                'linea1' => 'Se realizo una solicitud de generación de nueva llave para la cuenta con el nombre de ' . $user->nombre . ' y el correo electrónico ' . $user->email . '.',
                'linea2' => 'Para continuar con el proceso es necesario realizar alguna de las siguientes acciones:',
                'acceptAText' => 'Aprobar Admin',
                'acceptUText' => 'Aprobar Usuario',
                'declineText' => 'Declinar',
                'asunto' => 'Horus petición de recuperación de acceso',
            ];

            try {
                Mail::to($datos_correo['email'])->send(new NewUserRequest($datos_correo));
                if (count(Mail::failures()) > 0) {
                    return response()->json([
                        'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                        'type' => 'g-503a',
                        'error' => Mail::failures()
                    ], 502);
                }
            } catch (Exception $e1) {
                return response()->json([
                    'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                    'type' => 'g-503',
                    'error' => Mail::failures()
                ], 503);
            }
            $titulo = base64_encode('Solicitud Enviada');
            $text = base64_encode('La solicitud ha sido enviada al área correspondiente, en cuento se tenga una respuesta recibirá un correo.');
        } elseif(isset($request->reset) && !empty($request->reset) && $request->reset == 1) {
            return Redirect::to(self::URL . '/reset-key-form?token=' . $request->token);
        } else {
            $user->status = 1;
            $user->activation_token = '';
            $user->save();
            $titulo = base64_encode('Cancelada');
            $text = base64_encode('La solicitud fue cancelada, usted puede seguir usando su cuenta con la llave actual.');
        }

        return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
    }

    public function userResetApproval(Request $request)
    {
        $tokenArray = explode('[<>]', base64_decode(str_replace(" ","+",$request->token)));
        $user = User::where('activation_token', $tokenArray[0])->first();
        if (!$user) {
            $tipo = $tokenArray[1] == 1 ? 'aprobación' : 'declinación';
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link de ' . $tipo . ' es invalido, favor de comunicarse con el área de soporte para más información.');
            $button = base64_encode(1);
            $buttonText = base64_encode('Inicio');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text . '&button=' . $button . '&buttonText=' . $buttonText);
        }
        if ($tokenArray[1] == 1) {
            $user->activation_token = $this->temporalLinkToken();
            if($tokenArray[2] == 1){
                $user->tipo = 1;
            }
            $user->save();
            $user->notify(new ResetForm($user, self::URLB));
            $titulo = base64_encode('Autorizada');
            $text = base64_encode('La solicitud fue aprobada exitosamente, se envió un correo al usuario para informar de la actualización de su estado.');
        } else {
            $user->status = 1;
            $user->activation_token = '';
            $user->save();
            $user->notify(new DeclineAuthorization($user, 2));
            $titulo = base64_encode('Cancelada');
            $text = base64_encode('La solicitud fue cancelada, se envió un correo al usuario para informar de la actualización de su estado.');
        }
        return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text);
    }

    public function userResetData(Request $request)
    {
        $validar = Validator::make($request->all(), [
            'token'   => 'required|string',
            'password' => 'required|string',
        ]);
        if ($validar->fails()) {
            return response()->json([
                'message' => 'Error en los datos. Favor de verificarlos',
                'type' => 'g-401'
            ], 401);
        }
        $tokenArray = explode('[<>]', base64_decode($request->token));
        $user = User::where('activation_token', $tokenArray[0])->first();
        if (!$user) {
            $tipo = $tokenArray[1] == 1 ? 'aprobación' : 'declinación';
            $titulo = base64_encode('Invalido');
            $text = base64_encode('El link de ' . $tipo . ' es invalido, favor de comunicarse con el área de soporte para más información.');
            $button = base64_encode(1);
            $buttonText = base64_encode('Inicio');
            return Redirect::to(self::URL . '/activation?titulo=' . $titulo . '&text=' . $text . '&button=' . $button . '&buttonText=' . $buttonText);
        }
        $rsa = new RSA(null, null, null, self::OPENSSH);
        if (!$rsa->create()) {
            return response()->json([
                'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                'type' => 'g-501'
            ], 500);
        }
        $encrypted = $rsa->base64Encrypt($request->password);

        $user->password = $encrypted;
        $user->public_key  = $rsa->getPublicKeyFile();
        $user->private_key = $rsa->getPrivateKeyFile();
        $user->activation_token = $this->temporalLinkToken();
        if (!$user->save()) {
            return response()->json([
                'message' => 'Ocurrio un error en el servidor. Favor de contactar a soporte',
                'type' => 'g-502'
            ], 500);
        }
        $user->notify(new SignupActivate($user, self::URLB, 2));

        return response()->json([
            'message' => 'Nueva llave creada',
            'type' => '202'
        ], 202);
    }
}

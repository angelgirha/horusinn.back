<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class LectoresCron extends Model
{

    use HasFactory;
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $table = 'lectores_cron';

    protected $primaryKey = 'id';

    protected $fillable = [
        'nombre', 'estado','sentencia'
    ];


    public static function updateLectoresCron($cron)
    {
        $nuevoCron =  LectoresCron::where('id', '=',$cron['id'])->first();
        $nuevoCron->nombre = $cron['nombre'];
        $nuevoCron->estado = $cron['estado'];

        return $nuevoCron->save();
    }

    public static function createLectoresCron($cron)
    {
        $nuevoCron = new LectoresCron();
        $nuevoCron->nombre = $cron['nombre'];
        $nuevoCron->estado = $cron['estado'];
        if($nuevoCron->save()){
            return $nuevoCron;
        } else {
            return false;
        }
    }

}

<?php

namespace App\Models;

use DateTime;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

use function GuzzleHttp\json_decode;
use PDO;
use SebastianBergmann\Type\ObjectType;

class Ambientes extends Model
{
    use HasFactory;


    public static function f_getTablesName($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'mi_asistencia_hik'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("SELECT *, TABLE_NAME as Tables_name  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA= '" . $server_base . "' ");
    }

    public static function f_getFunciones($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("SHOW FUNCTION STATUS WHERE db = '" . $server_base . "'");
    }

    public static function f_getEvents($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->base_central;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("show events");
    }

    public static function f_getSP($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("SHOW PROCEDURE STATUS WHERE db = '" . $server_base . "'");
    }

    public static function f_getTriggers($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("SHOW triggers");
    }

    public static function f_getViews($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        return DB::select("SELECT *, TABLE_NAME as Tables_name  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA= '" . $server_base . "'  and TABLE_TYPE LIKE 'VIEW' ");
    }
                
    public static function f_getTareasAdicionales($server)
    {
        self::getServeInformation($server->producto, $serverInfo, 1);
        if ($server->producto == "liber" && $server->bd_origen == "_historico") {
            $server_base = $serverInfo->base . $server->bd_origen;
        } else if ($server->subproducto == "mi_asistencia_hik") {
            $server_base = "asishik_MiAsistenciaByHik_2001";
        } else if ($server->subproducto == "cloudclock") {
            $dbcentral = decrypt(base64_decode(DB::select("SELECT dbcentral FROM horus.servers_conexiones where producto = 'cloudclock'")[0]->dbcentral));
            $server_base = $dbcentral;
        } else {
            $server_base = $serverInfo->dbcentral;
        }
        self::setBaseConnection($serverInfo, $server_base);
        $tables = DB::select("SELECT *, TABLE_NAME as nombre,TABLE_NAME as tabla  FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA= '" . $server_base . "' and TABLE_TYPE LIKE 'BASE TABLE'  ");
        $i = 0;
        foreach ($tables as $table) {
            $total = DB::select("SELECT count(1) as total from " . $table->{'nombre'});
            if ($total[0]->{'total'} > 0) {
                $info[$i] = $table;
                $i++;
            }
        }
        return $info;
    }

    public static function f_getUltimosPasos($server)
    {
        try {
            self::getServeInformation($server, $serverInfo, 1);
            self::setBaseConnection($serverInfo, $serverInfo->dbcentral);
            return DB::select("SHOW PROCEDURE STATUS WHERE db = '" . $serverInfo->base_central . "' and Name = 'sp_actualiza_ip_clientes'");
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_actualiza_user($request, $conexionDB){

        Server::decodeServer($conexionDB,3);

        $con = new PDO('mysql:host=' . $conexionDB[0]->ip . '; port=' . $conexionDB[0]->puerto . '; dbname=cliente_'.$request['cliente'].'; ', $conexionDB[0]->usuario, $conexionDB[0]->password);

        $tblArray1 = [
            "1" => "correo",
            "2" => "asigna_empresa",
            "3" => "documentos",
            "4" => "empresas",
            "5" => "etiquetas",
            "6" => "parametros_otros",
            "7" => "parametros_rhino",
            "8" => "parametros_sp",
            "9" => "parametros_tarjeta_checadora",
            "10" => "parametros_vacaciones",
            "11" => "proceso_depuracion",
            "12" => "procesos_automatico",
            "13" => "procesos_traspaso",
            "14" => "procesos_traspaso_incidencias",
            "15" => "saldo_vacaciones",
            "16" => "series_folios",
            "17" => "sucursales",
            "18" => "tickets",
            "19" => "tickets_parametros_mesa",
            "20" => "tickets_titulos",
            "21" => "tipo_contrato",
            "22" => "tipo_nomina",
            "23" => "tipo_nota",
            "24" => "tipos_archivo",
            "25" => "tipos_falta",
            "26" => "tipo_sucursal",
            "27" => "tur_hor",
            "28" => "turno_empleado",
            "29" => "vacaciones",
            "30" => "perfil_user",
            "31" => "politicas_usuarios",
        ];

        $tblArray2 = [
            "32" => "sub_sub_menu_procesos_usu",
        ];

        for($i = 1; $i <= 31; $i++){
            if($i >= 1 && $i <= 31){
                $con->query('update cliente_'.$request['cliente'].'.'.$tblArray1[$i] .' set id_empresa = '. "'".$request['usuario']."'");
            }elseif($i == 32){
                $con->query('update cliente_'.$request['cliente'].'.'.$tblArray2[$i] .' set id_empresa = '. "'".$request['usuario']."', id_usuario = '".$request['usuario']."'");
            }
        }

        $con->query('INSERT INTO cliente_'.$request['cliente'].'.perfil_user (id_usuario, id_empresa, id_sucursal, id_user_alta, fecha_alta, id_user_mod, fecha_mod) VALUES ("'.$request['usuario'].'", "'.$request['usuario'].'", "001", "admin", now(), null, null)');
    }

    public static function f_creaPassAdmin($server, $conexionDB){
        $serverInfo = (object) '';
        $serverInfo->ip = $conexionDB[0]->ip;
        $serverInfo->puerto = $conexionDB[0]->puerto;
        $serverInfo->usuario = $conexionDB[0]->usuario;
        $serverInfo->password = $conexionDB[0]->password;
        $serverInfo->base_central = 'cliente_'.$server['cliente'];

        $password = 'Admin0310+'.$server['cliente'];
        self::f_encripta($password, 22, $password_enc, $msg_encripta);

        self::setBaseConnection($serverInfo, $serverInfo->base_central);
        DB::statement("update cliente_".$server['cliente'].".usuarios set password = '".$password_enc."' where id_usuario = 'admin'");
    }

    public static function f_execProcedure($server)
    {
        $conexionDB = DB::select('SELECT servers.id, nombre, ip, ip_local, servers.puerto, servers.usuario, servers.password, servers.producto,  servers_conexiones.centraliza FROM servers INNER JOIN servers_conexiones ON servers.id = server_id WHERE nombre = "'.$server['server_name'].'"');

        $server->producto == 'girha' || $server->producto == 'biotime_cloud' ? Ambientes::f_actualiza_user($server, $conexionDB) : '' ;

        try {
            self::getServeInformation($server->server_name, $serverInfo1);
            self::getServeInformation($server->server_2, $serverInfo2);
            self::getServeInformation($server->producto, $serverInfo, 1);
            self::setBaseConnection($serverInfo, $serverInfo->base_central);

            DB::statement('SET @codigo=null');
            DB::statement('SET @msj=null');
            DB::statement('call ' . $server->name . '(' . $server->cliente . ', "' . $serverInfo1->ip_local . '", "' . $serverInfo2->ip_local . '", @codigo, @msj)');
            $respuesta = DB::select('select @codigo, @msj');

            Ambientes::f_creaPassAdmin($server, $conexionDB);

            return $respuesta;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_validacionEmpresa($server)
    {
        try {
            self::getServeInformation($server->producto, $serverInfo, 1);
            if ($server->producto == 'girha' || $server->producto == 'biotime_cloud') {
                self::setBaseConnection($serverInfo, $serverInfo->base_central);
                $query = "select * from configuracion_inicial where descripcion_emp = '" . $server->descEmpresa . "'";
            } else if ($server->producto == 'liber') {
                self::setBaseConnection($serverInfo, $serverInfo->base_central);
                $query = "select * from clientes where nombre_fiscal = '" . $server->descEmpresa . "'";
            } else {
                switch ($server->subproducto) {
                    case "mi_asistencia_hik":
                        self::setBaseConnection($serverInfo, "asishik_MiAsistenciaByHik");
                        break;
                    case "cloudclock":
                        self::setBaseConnection($serverInfo, "cloudclo_CloudClock");
                        break;
                    default:
                        self::setBaseConnection($serverInfo, $serverInfo->base_central);
                        break;
                }
                $query = "select * from EMPRESAS where RFC = '" . strtoupper($server->codigoEmpresa) . "' or NOMBRE = '" . $server->descEmpresa . "'";
            }
            return DB::select($query);
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_getEmpleados($info){
        $conexionDB = DB::select('SELECT servers.id, nombre, ip, ip_local, servers.puerto, servers.usuario, servers.password, servers.producto,  servers_conexiones.centraliza FROM servers INNER JOIN servers_conexiones ON servers.id = server_id WHERE nombre = "'.$info['server_name'].'"');

        Server::decodeServer($conexionDB,3);

        $con = new PDO('mysql:host=' . $conexionDB[0]->ip . '; port=' . $conexionDB[0]->puerto . '; dbname=Girha_General; ', $conexionDB[0]->usuario, $conexionDB[0]->password);

        $con->query("CALL Girha_General.sp_empleados_status()");
        $respuestaDB = $con->query("select sum(activo) as total from empleados_clientes");
        $totalEmpleados = $respuestaDB->fetchAll(PDO::FETCH_ASSOC);

        return $totalEmpleados[0]['total'];
    }

    public static function f_getLectores($info){
        $conexionDB = DB::select('SELECT servers.id, nombre, ip, ip_local, servers.puerto, servers.usuario, servers.password, servers.producto,  servers_conexiones.centraliza FROM servers INNER JOIN servers_conexiones ON servers.id = server_id WHERE nombre = "'.$info['server_2'].'"');

        Server::decodeServer($conexionDB,3);

        $con = new PDO('pgsql:host=' . $conexionDB[0]->ip . '; port=' . $conexionDB[0]->puerto . '; dbname=engine; ', $conexionDB[0]->usuario, $conexionDB[0]->password);

        $respuestaDB = $con->prepare("select count(1) as total from iclock_dispositivos_empresas");
        $respuestaDB->execute();
        $data = $respuestaDB->fetchAll( PDO::FETCH_ASSOC);
        $respuestaDB->closeCursor();

        return $data[0]['total'];
    }

    public static function f_insertInfoClienteDirectorio($request){
        if($request["producto"] == 'girha' || $request["producto"] == 'biotime_cloud'){
            $query = "INSERT INTO horus.clientes (id, id_cliente, rfc, razon_social, domicilio, ambiente, created_at, updated_at) values ("."null, '".$request["cliente"]."', '".base64_encode(encrypt($request["rfc"]))."', '".$request["descEmpresa"]."', '".base64_encode(encrypt($request["dirEmpresa"]))."', '".$request["producto"]."', now(), null".")";
            DB::statement($query);

            $nombre2 = isset($request["contacto2"]) ? $request["contacto2"] : null ;
            $nombre3 = isset($request["contacto3"]) ? $request["contacto3"] : null ;

            $tipo2 = isset($request["tipoContacto2"]) ? $request["tipoContacto2"]["name"] : null ;
            $tipo3 = isset($request["tipoContacto3"]) ? $request["tipoContacto3"]["name"] : null ;

            $correo1 = isset($request["email_contacto1"]) ? base64_encode(encrypt($request["email_contacto3"])) : null ;
            $correo2 = isset($request["email_contacto2"]) ? base64_encode(encrypt($request["email_contacto3"])) : null ;
            $correo3 = isset($request["email_contacto3"]) ? base64_encode(encrypt($request["email_contacto3"])) : null ;

            $telefono1 = isset($request["telefono1"]) ? base64_encode(encrypt($request["telefono1"])) : null ;
            $telefono2 = isset($request["telefono2"]) ? base64_encode(encrypt($request["telefono2"])) : null ;
            $telefono3 = isset($request["telefono3"]) ? base64_encode(encrypt($request["telefono3"])) : null ;

            $query = "INSERT INTO horus.directorio (id, id_cliente, tipo_contacto1, nombre1, correo1, telefono1, tipo_contacto2, nombre2, correo2, telefono2, tipo_contacto3, nombre3, correo3, telefono3, created_at, updated_at) values ("."null, '".$request["cliente"]."', '".$request["tipoContacto1"]["name"]."', '".$request["contacto1"]."', '".$correo1."', '".$telefono1."', '".$tipo2."', '".$nombre2."', '".$correo2."', '".$telefono2."', '".$tipo3."', '".$nombre3."', '".$correo3."', '".$telefono3."', now(), null".")";
            DB::statement($query);
        }
    }

    public static function f_getLastCliente($server, $producto = '')
    {
        self::getServeInformation($producto, $serverCentral, 1);

        $conexionDB = DB::select('SELECT servers.id, nombre, ip, ip_local, servers.puerto, servers.usuario, servers.password, servers.producto,  servers_conexiones.centraliza FROM servers INNER JOIN servers_conexiones ON servers.id = server_id WHERE servers_conexiones.centraliza = 1');

        Server::decodeServer($conexionDB,3);

        $con = new PDO('mysql:host=' . $conexionDB[0]->ip . '; port=' . $conexionDB[0]->puerto . '; dbname=obelsys; ', $conexionDB[0]->usuario, $conexionDB[0]->password);

        if (key_exists('subproducto', $server) && isset($server['subproducto']) && $server['subproducto'] != "mi_asistencia") {
            if ($server['subproducto'] == 'mi_asistencia_hik') {
                self::setBaseConnection($serverCentral, "asishik_MiAsistenciaByHik");
                $query = "select max(ID_INDEX) as total from EMPRESAS";
            } else if ($server['subproducto'] == 'cloudclock') {
                self::setBaseConnection($serverCentral, "cloudclo_CloudClock");
                $query = "select max(ID_INDEX) as total from EMPRESAS";
            }
        } else {
            self::setBaseConnection($serverCentral, $serverCentral->base_central);
            if ($producto == 'girha') {
                //$respuestaDB = $con->query("select max(id_cliente) as total from obelsys.clientes where id_cliente between 1000 and 10000");
                $respuestaDB = $con->query("select max(id_cliente) as total from obelsys.clientes where (id_cliente between 1000 and 10000) AND (id_cliente < 1254)");
            } elseif ($producto == 'biotime_cloud') {
                $respuestaDB = $con->query("select max(id_cliente) as total from obelsys.clientes where id_cliente between 1000 and 10000");
            } elseif ($producto == 'liber') {
                $respuestaDB = $con->query("select max(id_cliente) as total from obelsys.clientes where id_cliente >= 7000");
            } else if ($producto == 'mi_asistencia') {
                if (key_exists('bd_origen', $server) && isset($server['bd_origen']) && $server['bd_origen'] == "2") {
                    $query = "select max(ID_INDEX) as total from EMPRESAS where ID_INDEX between 700 and 899";
                } else {
                    $query = "select max(ID_INDEX) as total from EMPRESAS where ID_INDEX between 500 and 699";
                }
            }
        }

        $resp = $respuestaDB->fetchAll(PDO::FETCH_ASSOC);
        $respuestaDB->closeCursor();
        $resp[0]["total"] = intval($resp[0]["total"]);
        return $resp;

        //return DB::select($query);
    }
    public static function f_getAllClientes($server, $producto = '')
    {
        self::getServeInformation($producto, $serverCentral, 1);

        $conexionDB = DB::select('SELECT servers.id, nombre, ip, ip_local, servers.puerto, servers.usuario, servers.password, servers.producto,  servers_conexiones.centraliza FROM servers INNER JOIN servers_conexiones ON servers.id = server_id WHERE servers_conexiones.centraliza = 1');

        Server::decodeServer($conexionDB,3);
        $con = new PDO('mysql:host=' . $conexionDB[0]->ip . '; port=' . $conexionDB[0]->puerto . '; dbname=obelsys; ', $conexionDB[0]->usuario, $conexionDB[0]->password);

        if (key_exists('subproducto', $server) && isset($server['subproducto']) && $server['subproducto'] != "mi_asistencia") {
            if ($server['subproducto'] == 'mi_asistencia_hik') {
                self::setBaseConnection($serverCentral, "asishik_MiAsistenciaByHik");
                $query = "select ID_INDEX from EMPRESAS";
            } else if ($server['subproducto'] == 'cloudclock') {
                self::setBaseConnection($serverCentral, "cloudclo_CloudClock");
                $query = "select ID_INDEX from EMPRESAS";
            }
        } else {
            self::setBaseConnection($serverCentral, $serverCentral->base_central);
            if ($producto == 'girha') {
                $respuestaDB = $con->query("select c.id_cliente as code, concat('cliente_', c.id_cliente) as name, descripcion_emp from clientes c join configuracion_inicial ci on c.id_cliente = ci.id_cliente where c.id_cliente between 1000 and 10000");
            } elseif ($producto == 'biotime_cloud') {
                $respuestaDB = $con->query("select c.id_cliente as code, concat('cliente_', c.id_cliente) as name, descripcion_emp from clientes c join configuracion_inicial ci on c.id_cliente = ci.id_cliente where c.id_cliente between 1000 and 10000");
            } elseif ($producto == 'liber') {
                $respuestaDB = $con->query("select c.id_cliente as code, concat('cliente_', c.id_cliente) as name, descripcion_emp from clientes c join configuracion_inicial ci on c.id_cliente = ci.id_cliente  where c.id_cliente >= 7000");
            } else if ($producto == 'mi_asistencia') {
                if (key_exists('bd_origen', $server) && isset($server['bd_origen']) && $server['bd_origen'] == "2") {
                    $query = "select ID_INDEX from EMPRESAS where ID_INDEX between 700 and 899";
                } else {
                    $query = "select ID_INDEX from EMPRESAS where ID_INDEX between 500 and 699";
                }
            }
        }
        $return = $respuestaDB->fetchAll(PDO::FETCH_ASSOC);
        $respuestaDB->closeCursor();
        return $return;

        //return DB::select($query);
    }


    public static function f_getClienteDatos($server, $producto = '')
    {
        self::getServeInformation($producto, $serverCentral, 1);
        if (key_exists('subproducto', $server) && isset($server['subproducto']) && $server['subproducto'] != "mi_asistencia") {
            if ($server['subproducto'] == 'mi_asistencia_hik') {
                self::setBaseConnection($serverCentral, "asishik_MiAsistenciaByHik");
                $query = "select ID_INDEX from EMPRESAS";
            } else if ($server['subproducto'] == 'cloudclock') {
                self::setBaseConnection($serverCentral, "cloudclo_CloudClock");
                $query = "select ID_INDEX from EMPRESAS";
            }
        } else {
            self::setBaseConnection($serverCentral, $serverCentral->base_central);
            if ($producto == 'girha' || $producto == 'biotime_cloud') {
                $query = "select id_cliente as code, concat('cliente_', id_cliente) as name  from clientes where id_cliente between 1000 and 10000";
            } elseif ($producto == 'liber') {
                $query = "select id_cliente as code, concat('cliente_', id_cliente) as name from clientes where id_cliente >= 7000";
            } else if ($producto == 'mi_asistencia') {
                if (key_exists('bd_origen', $server) && isset($server['bd_origen']) && $server['bd_origen'] == "2") {
                    $query = "select ID_INDEX from EMPRESAS where ID_INDEX between 700 and 899";
                } else {
                    $query = "select ID_INDEX from EMPRESAS where ID_INDEX between 500 and 699";
                }
            }
        }
        return DB::select($query);
    }




    public static function f_creaBase($bd_nueva, $server)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
            } else if ($server->subproducto == 'cloudclock') {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
            } else { // Marcador
                self::getBaseNuevaName($serverInfo, $bd_nueva);
            }
            self::setBaseConnection($serverInfo, '');
            if (DB::statement('create database ' . $bd_nueva)) {
                return ['result' => true, 'name' => $bd_nueva];
            } else {
                return ['result' => false, 'name' => 'Error al crear la base'];
            }
        } catch (Exception $e) {
            return ['result' => false, 'name' => $e->getMessage()];
        }
    }



    public static function f_creaTablas($server, $bd_nueva)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createTable = DB::select("show create table `" . $server->name . "`");

            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement('SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0');
            DB::statement('SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0');
            DB::statement("SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO'");
            DB::statement('SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0');
            if (isset($createTable[0]->{'Create Table'})) {
                DB::statement($createTable[0]->{'Create Table'});
            } else {
                self::setBaseConnection($serverInfoCenter, $server_base);
                $viewColums = DB::select("SHOW COLUMNS FROM `" . $server->name . "`");
                $temporalContent = "";
                $numItems = count($viewColums);
                $i = 0;
                foreach ($viewColums as $column) {
                    $coma = ++$i === $numItems ? " " : ", ";
                    $temporalContent .= " 1 AS " . $column->{'Field'} . $coma;
                }
                $temporalStructure = "CREATE VIEW " . $server->name . " AS SELECT " . $temporalContent;
                self::setBaseConnection($serverInfo, $bd_nueva);
                DB::statement($temporalStructure);
            }

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_creaFunciones($bd_nueva, $server, $name)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createFunction = DB::select("SHOW CREATE FUNCTION `" . $name . "`");
            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement("SET sql_mode = '" . $createFunction[0]->{'sql_mode'} . "'");
            DB::unprepared($createFunction[0]->{'Create Function'});

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_creaEvents($bd_nueva, $server, $name)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createFunction = DB::select("SHOW CREATE EVENT `" . $name . "`");

            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement("SET sql_mode = '" . $createFunction[0]->{'sql_mode'} . "'");
            DB::unprepared($createFunction[0]->{'Create Event'});

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_creaSP($bd_nueva, $server, $name)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createFunction = DB::select("SHOW CREATE PROCEDURE `" . $name . "`");
            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement("SET sql_mode = '" . $createFunction[0]->{'sql_mode'} . "'");
            if ($server->producto == "liber") {
                $createFunction[0]->{'Create Procedure'} = str_replace("liber_11_historico", "liber_" . $server->cliente . "_historico", $createFunction[0]->{'Create Procedure'});
                DB::unprepared($createFunction[0]->{'Create Procedure'});
            } else {
                DB::unprepared($createFunction[0]->{'Create Procedure'});
            }

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }
    public static function f_creaTriggers($bd_nueva, $server, $name)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createFunction = DB::select("SHOW CREATE TRIGGER `" . $name . "`");
            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement("SET sql_mode = '" . $createFunction[0]->{'sql_mode'} . "'");
            $trigger = str_replace($server_base, $bd_nueva, $createFunction[0]->{'SQL Original Statement'});
            DB::unprepared($trigger);
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_creaViews($bd_nueva, $server, $name)
    {
        try {
            self::getServeInformation($server->server_name, $serverInfo);
            self::getServeInformation($server->producto, $serverInfoCenter, 1);
            if ($server->producto == "liber" && $server->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $server->bd_origen;
            } else if ($server->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $server->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($server->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $server->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            $createFunction = DB::select("SHOW CREATE view `" . $name . "`");
            self::setBaseConnection($serverInfo, $bd_nueva);
            DB::statement("DROP VIEW IF EXISTS " . $name);
            DB::unprepared($createFunction[0]->{'Create View'});

            return true;
        } catch (Exception $e) {
            return $e;
        }
    }


    public static function f_creaInformacionDefault($bd_nueva, $server, $name, $request)
    {
        try {
            self::getServeInformation($server, $serverInfo);
            self::getServeInformation($request->producto, $serverInfoCenter, 1);
            if ($request->producto == "liber" && $request->bd_origen == "_historico") {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base . $request->bd_origen;
            } else if ($request->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $request->cliente;
                $server_base = "asishik_MiAsistenciaByHik_2001";
            } else if ($request->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $request->cliente;
                $server_base = "cloudclo_CloudClock_0";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $server_base = $serverInfo->base;
            }
            self::setBaseConnection($serverInfoCenter, $server_base);
            if ($request->producto == 'girha' || $request->producto == 'biotime_cloud') {
                if ($name == 'codigo_pos') {
                    ini_set('max_execution_time', 90);
                    ini_set('memory_limit', '256M');
                }
                $registros = DB::select('select * from ' . $server_base . '.' . $name);
                $cont = 1;
                $registroN = '';
                $registrosN = array();
                foreach ($registros as $registro) {
                    $registro = json_decode(json_encode($registro), true);
                    $cont2 = 1;
                    $registroN .= "(";
                    foreach ($registro as $key => $valor) {
                        if ($valor === NULL) {
                            $registroN .= "NULL";
                        } else if (is_string($valor)) {
                            if (str_contains($valor, "'")) {
                                $valor = str_replace("'", "\'", $valor);
                                $registroN .= "'" . $valor . "'";
                            } else {
                                $registroN .= "'" . $valor . "'";
                            }
                        } else {
                            $registroN .= $valor;
                        }
                        if ($cont2 == count($registro)) {
                            $registroN .= ")";
                        } else {
                            $registroN .= ",";
                            $cont2++;
                        }
                    }
                    if ($cont == count($registros) || $cont % 5000 == 0) {
                        $registroN .= ";";
                        array_push($registrosN, $registroN);
                        $registroN = '';
                        $cont++;
                    } else {
                        $registroN .= ",";
                        $cont++;
                    }
                }
                self::setBaseConnection($serverInfo, $bd_nueva);
                //DB::statement("delete from " . $bd_nueva . "." . $name);
                DB::statement("alter table " . $bd_nueva . "." . $name . " auto_increment = 1");
                foreach ($registrosN as $valores) {
                    DB::statement("SET FOREIGN_KEY_CHECKS=0;");
                    DB::statement("ALTER TABLE " . $bd_nueva . "." . $name . " DISABLE KEYS;");
                    if($name == 'parametros_sp' || $name == 'parametros_vacaciones'){
                        DB::update('update '.$bd_nueva.'.'.$name.' set id_empresa = "'.$request->usuario.'"');
                    }elseif($name == 'tipos_archivo'){
                        DB::statement("delete from " . $bd_nueva . "." . $name);
                        DB::insert('insert into ' . $bd_nueva . '.' . $name . ' values ' . $valores);
                    }else{
                        DB::insert('insert into ' . $bd_nueva . '.' . $name . ' values ' . $valores);
                    }
                    DB::statement("ALTER TABLE " . $bd_nueva . "." . $name . " ENABLE KEYS;");
                }
            } else {
                //DB::statement("delete from " . $bd_nueva . "." . $name);
                DB::statement("alter table " . $bd_nueva . "." . $name . " auto_increment = 1");
                if ($name != "CAT_USUARIOS") {
                    $insert = DB::getPdo()->exec(
                        "SET FOREIGN_KEY_CHECKS=0;
                        ALTER TABLE " . $bd_nueva . "." . $name . " DISABLE KEYS;
                        INSERT INTO " . $bd_nueva . "." . $name . " SELECT * FROM " . $server_base . "." . $name . ";
                        ALTER TABLE " . $bd_nueva . "." . $name . " ENABLE KEYS;"
                    );
                }
            }
            if ($serverInfoCenter->producto == 'girha' || $serverInfoCenter->producto == 'biotime_cloud') {
                $viewColums = DB::select("SHOW COLUMNS FROM " . $bd_nueva . "." . $name);
                $columna = '';
                foreach ($viewColums as $column) {
                    if ($column->{'Field'} == 'id_empresa') {
                        $columna = $column->{'Field'};
                        break;
                    }
                }
                if ($columna == 'id_empresa') {
                    $query = "UPDATE " . $bd_nueva . "." . $name . " SET id_empresa = '" . $request->codigoEmpresa . "';";
                    DB::statement($query);
                }
            }
            return true;
        } catch (Exception $e) {
            return $e;
        }
    }

    public static function f_insertNewCliente($bd_nueva, $server, $name, $request)
    {
        $id_cliente = $bd_nueva;
        try {
            self::getServeInformation($server, $serverInfo);
            self::getServeInformation($request->producto, $serverCentral, 1);
            if ($request->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $request->cliente;
                $base_central = "asishik_MiAsistenciaByHik";
            } else if ($request->subproducto == "cloudclock") {
                $bd_nueva = "cloudclo_CloudClock_" . $request->cliente;
                $base_central = "cloudclo_CloudClock";
            } else {
                self::getBaseNuevaName($serverInfo, $bd_nueva);
                $base_central = $serverInfo->base_central;
            }
            self::setBaseConnection($serverCentral, $base_central);
            if ($request->producto == 'girha' || $request->producto == 'biotime_cloud') {
                $query = "delete from clientes where id_cliente = " . $id_cliente;
                DB::statement($query);
                $query = "INSERT INTO clientes (id_cliente,id_empleados, mail,fecha_ini_url,fecha_fin_url,status,fecha_alta,confirmacion) values (" . $id_cliente . ",'RH" . $id_cliente . "','" . $request->descEmpresa . "',now(),now(),1,now(),'');";
                DB::statement($query);
                $query = "delete from configuracion_inicial where id_cliente = " . $id_cliente;
                DB::statement($query);
                $query = "INSERT INTO configuracion_inicial (id_cliente,
                                            id_empresa,
                                            descripcion_emp,
                                            id_sucursal,
                                            descripcion_suc,
                                            usuario,
                                            password,
                                            status,
                                            fecha_alta)
                                            values (" . $id_cliente . ",
                                            '" . $request->usuario . "',
                                            '" . $request->descEmpresa . "',
                                            '" . $request->codigoSucursal . "',
                                            '" . $request->descSucursal . "',
                                            '" . $request->usuario . "',
                                            '" . $request->password . "',
                                            3,
                                            now());";
                DB::statement($query);

                self::setBaseConnection($serverInfo, $bd_nueva);
                $query = "UPDATE empresas SET
                                id_empresa='" . $request->usuario . "',
                                razon_social='" . $request->descEmpresa . "',
                                rfc='',
                                regimen_fiscal=''";
                DB::statement($query);
                $query = "UPDATE sucursales SET
                                descripcion='Todos',
                                contacto='',
                                mail_contacto=''";
                DB::statement($query);
                DB::statement("delete from usuarios where id_usuario = '".$request->usuario."'");
                DB::statement("alter table usuarios auto_increment = 1");
                self::f_encripta($request->password, 22, $password_enc, $msg_encripta);
                $query = "INSERT INTO usuarios (id_usuario, nombre,password, mail, admin, status,fecha_alta) VALUE ";
                $query .= "('" . $request->usuario . "','" . $request->contacto1 . "','" . $password_enc . "','" . $request->email_contacto1 . "',1,1,NOW())";
                DB::statement($query);
            } elseif ($request->producto == 'liber') {
                $query = "INSERT INTO clientes (id_cliente,nombre_base_datos, nombre_fiscal,SF_Ticket) values (" . $id_cliente . ",'" . $bd_nueva . "','" . $request->descEmpresa . "',0);";
                DB::statement($query);
            } else {
                /*$query = "delete from EMPRESAS where ID_INDEX = " . $id_cliente;
                DB::statement($query);*/
                $query = "INSERT INTO EMPRESAS (ID_INDEX, RFC, NOMBRE, DIRECCION, CONTACTO, TELEFONO, CORREO_ELECTRONICO,STATUS, VERIFICADO,";
                $query .= " PRIVACIDAD, CONTRATO, EMPLEADOS_ADICIONALES, TAMANIO_ADICIONAL)";
                $query .= " VALUES (" . $id_cliente . ", '" . $request->codigoEmpresa . "', '" . $request->descEmpresa . "', '" . $request->dirEmpresa . "', '" . $request->contacto . "', '" . $request->telefono . "', '" . $request->email_contacto . "', ";
                $query .= "1, 0, 0, 0, 0, 0)";
                DB::statement($query);
                self::setBaseConnection($serverInfo, $bd_nueva);
                //DB::statement("delete from CAT_USUARIOS");
                DB::statement("alter table CAT_USUARIOS auto_increment = 1");
                $query = "INSERT INTO CAT_USUARIOS (ID_USUARIO, CONTRASENA, ASIGNADO, TIPO, EMAIL, USUARIO_ALTA, FECHA_ALTA) ";
                $query .= " VALUES ('" . $request->usuario . "', '" . $request->password . "','Admin',1,'" . $request->email_contacto . "',1,NOW());";
                DB::statement($query);
            }
            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function f_insetsLiber($bd_nueva, $server, $request)
    {
        try {
            $id_cliente = $bd_nueva;
            self::getServeInformation($server, $serverInfo);
            self::getBaseNuevaName($serverInfo, $bd_nueva);

            //en liber
            self::setBaseConnection($serverInfo, $serverInfo->base_central);
            $query = "INSERT INTO users ( login_usuario, email, password, client, fecha_alta)";
            $query2 = $query . " values ('" . $request->usuario . "', '" . $request->usuario_email . "', '" . Hash::make($request->password) . "', " . $id_cliente . ", now());";
            DB::statement($query2);
            $query = "SELECT id from users where email = '" . $request->usuario_email . "' limit 1";
            $result = DB::select($query);
            $certificado = self::addCerLiber($request, $id_cliente);
            if (is_string($certificado)) {
                return $certificado;
            }

            $lastInserts = self::f_lastInserts($bd_nueva, $serverInfo, $request, $result[0]->id);
            if (is_string($lastInserts)) {
                return $lastInserts;
            } else {
                return true;
            }
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    private static function f_lastInserts($bd_nueva, $serverInfo, $request, $id_user)
    {
        try {
            //en la base generada
            self::setBaseConnection($serverInfo, $bd_nueva);
            //crear sucursales
            $query = "truncate table sucursales ;";
            DB::statement($query);
            $query = "INSERT INTO sucursales (id_consecutivo_origen_dato, id_clave, descripcion, usuario_movimiento,fecha_movimiento) ";
            $query2 = $query . "VALUES (0,'" . $request->codigoSucursal . "', 'Todos'," . $id_user . ", now());";
            DB::statement($query2);

            //insert en tabla empresas
            $query = "truncate table empresas;";
            DB::statement($query);
            $query = "INSERT INTO empresas (id_consecutivo_origen_dato, rfc, razon_social,email,calle,cp, usuario_movimiento) ";
            $query2 = $query . "VALUES (0, '" . $request->codigoEmpresa . "', '" . $request->descEmpresa . "', '" . $request->email_contacto . "', ";
            $query3 = $query2 . "'" . $request->calle . "', '" . $request->cp . "', " . $id_user . ");";
            DB::statement($query3);

            //inserts user
            //DB::statement("delete from users where id_consecutivo_empleado = " . $id_user);
            $query = "INSERT INTO users ( id,id_consecutivo_origen_dato, login_usuario, id_consecutivo_empleado, email, password) select  id,0, login_usuario," . $id_user . ", email, password from " . $serverInfo->base_central . ".users where " . $serverInfo->base_central . ".users.id = " . $id_user;
            DB::statement($query);

            //insert terminales
            //DB::statement("delete from terminales where id_clave ='" . $request->codigoTerminal . "'");
            $query = "INSERT INTO terminales (id_consecutivo_origen_dato, id_clave, numero_serie, ip_address, mac_address, modelo, usuario_movimiento) ";
            $query2 = $query . "values (0,'" . $request->codigoTerminal . "', '" . $request->descTerminal . "', '" . $request->ipTerminal . "','" . $request->mac . "','" . $request->modelo . "', " . $id_user . ")";
            DB::statement($query2);
            $terminal = DB::select('select id_consecutivo from terminales where id_clave = "' . $request->codigoTerminal . '"');

            //tabla users_terminales
            DB::statement("truncate table users_terminales");
            $query = "insert into users_terminales (id_consecutivo_usuario, id_consecutivo_terminal) ";
            $query2 = $query . "values (" . $id_user . ", " . $terminal[0]->id_consecutivo . ")";
            DB::statement($query2);

            //inserts modulos
            $insert = DB::getPdo()->exec(
                "SET FOREIGN_KEY_CHECKS=0;
                ALTER TABLE modulos DISABLE KEYS;
                INSERT INTO modulos SELECT * FROM " . $serverInfo->base . ".modulos;
                ALTER TABLE modulos ENABLE KEYS;"
            );
            //inserts usuarios_modulos
            DB::statement("alter table users_modulos auto_increment = 1");
            $query = "insert into users_modulos (id_consecutivo_usuario, id_consecutivo_modulo) ";
            $query2 = $query . "select " . $id_user . ", id_consecutivo from " . $serverInfo->base . ".modulos";
            DB::statement($query2);

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function f_destruyeCLiente($ambiente, $serverCen)
    {
        Ambientes::getBaseConnection($defaultDb , "mysql");
        DB::update('UPDATE horus.clientes SET rfc = null, domicilio = null, ambiente = null, deleted_at = now() WHERE id_cliente = '.$ambiente->cliente);
        DB::delete('DELETE FROM horus.directorio where id_cliente = '.$ambiente->cliente);
        $serverBDaEliminar = Server::where('servers.producto', $ambiente->producto)->where('servers_conexiones.centraliza', 0)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->get();
        $servermysqlDB = Lectores::getServerMysqlBD($serverBDaEliminar, $ambiente->ip_mysql);
        $serverpostgreDB = Lectores::getServerPostegreBD($serverBDaEliminar, $ambiente->ip_psql);

        try {
            if ($ambiente->subproducto == "mi_asistencia_hik") {
                $bd_nueva = "asishik_MiAsistenciaByHik_" . $ambiente->cliente;
            } else if ($ambiente->subproducto == 'cloudclock') {
                $bd_nueva = "cloudclo_CloudClock_" . $ambiente->cliente;
            } else {
                $bd_nueva = $ambiente->name;
            }

            self::setBaseConnection($serverCen, $serverCen->base_central);
            if ($ambiente->producto == 'girha' || $ambiente->producto == 'biotime_cloud') {
                $query = "UPDATE clientes SET status = 0  where id_cliente = " . $ambiente->cliente. ";";
                DB::update($query);
            }

            self::setBaseConnection($servermysqlDB, $bd_nueva);
            if (DB::delete('DROP DATABASE IF EXISTS ' . $bd_nueva)) {
                self::setBaseConnection($serverCen, $serverCen->base_central);
                DB::update("UPDATE obelsys.clientes SET id_empleados = '', ip_mysql = null, ip_postgresql = null WHERE id_cliente = ".$ambiente->cliente);
                DB::delete('DELETE FROM obelsys.configuracion_inicial where id_cliente = '.$ambiente->cliente);
                DB::delete('DELETE FROM obelsys.clientes_llaves where id_cliente = '.$ambiente->cliente);
                /*self::setBaseConnection($serverpostgreDB, $serverpostgreDB->base_central);*/
                return ['result' => true, 'name' => $bd_nueva];
            } else {
                return ['result' => false, 'name' => 'Error al elimibar la base'];
            }
            return ['result' => true, 'name' => $bd_nueva];
        } catch (Exception $e) {
            return ['result' => false, 'name' => $e->getMessage()];
        }
    }




    public static function getServeInformation($serOprod, &$serverInfo, $centralizadora = 0)
    {
        $serverInfo = Server::getServerInfo($serOprod, $centralizadora);
    }


    public static function getBaseNuevaName($serverInfo, &$bd_name)
    {
        $bd_name = $serverInfo->prefix_base . $bd_name;
    }

    public static function setBaseConnection($server, $bd_name, $conexion = "mysql")
    {
        switch ($conexion) {
            case "mysql":
                Config::set('database.default', "mysql");
                Config::set('database.connections.mysql.host', $server->ip);
                Config::set('database.connections.mysql.port', $server->puerto);
                Config::set('database.connections.mysql.database', $bd_name);
                Config::set('database.connections.mysql.username', $server->usuario);
                Config::set('database.connections.mysql.password', $server->password);
                DB::reconnect();
                break;
            case "pgsql":
                Config::set('database.default', "pgsql");
                Config::set('database.connections.pgsql.host', $server->ip);
                Config::set('database.connections.pgsql.port', $server->puerto);
                Config::set('database.connections.pgsql.database', $bd_name);
                Config::set('database.connections.pgsql.username', $server->usuario);
                Config::set('database.connections.pgsql.password', $server->password);
                DB::reconnect();
                break;
        }
    }

    public static function getBaseConnection(&$conexionDb, $conexion = "mysql")
    {
        $conexionDb = (object) [
            'ip' => '',
            'puerto' => '',
            'bd_name' => '',
            'usuario' => '',
            'password' => '',

        ];

        switch ($conexion) {
            case "mysql":
                $conexionDb->ip = Config::get('database.connections.mysql.host');
                $conexionDb->puerto = Config::get('database.connections.mysql.port');
                $conexionDb->bd_name = Config::get('database.connections.mysql.database');
                $conexionDb->usuario = Config::get('database.connections.mysql.username');
                $conexionDb->password = Config::get('database.connections.mysql.password');
                break;
            case "pgsql":
                $conexionDb->ip = Config::get('database.connections.pgsql.host');
                $conexionDb->puerto = Config::get('database.connections.pgsql.port');
                $conexionDb->bd_name = Config::get('database.connections.pgsql.database');
                $conexionDb->usuario = Config::get('database.connections.pgsql.username');
                $conexionDb->password = Config::get('database.connections.pgsql.password');
                break;
        }
    }


    public static function addCerLiber($request, $client)
    {
        //informacion de liber
        try {
            $fromKey = base64_decode("6MWtFEEIzJ4CvpnNZEsh/V624mFeCCfEWpPtMSeLfbg=");
            $cipher = "AES-256-CBC";
            $encrypter = new Encrypter($fromKey, $cipher);
            $key_c = $encrypter->encrypt("liber");
            $status = $encrypter->encrypt(1);
            $user_login = $encrypter->encrypt($request->usuario);
            $client = $encrypter->encrypt($client);
            $fecha_alta = $encrypter->encrypt((new DateTime('now'))->format('Y-m-d H:i:s'));

            $query = "INSERT INTO certificates
                            ( key_cer,
                            estatus,
                            user_login,
                            client,
                            fecha_alta)
                             values (
                            '" . $key_c . "',
                            '" . $status . "',
                            '" . $user_login . "',
                            '" . $client . "',
                            '" . $fecha_alta . "'
                            );";
            DB::statement($query);

            return true;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }

    public static function updateCer($request, $client)
    {
        return        $encryptedToString = $encrypterTo->encrypt('hola');
        self::getServeInformation('liber', $serverInfo);
        $bd_nueva = '6';
        self::getBaseNuevaName($serverInfo, $bd_nueva);
        self::setBaseConnection($serverInfo, $serverInfo->base_centra);

        $client = $encrypter->encrypt(6);
        $user = encrypt('zanzakigus');

        $query = "update certificates set
                            client =
                            '" . $client . "'
                             login_user = '" . $user . "' where id_consecutivo = 12;";
        DB::statement($query);

        return true;
    }


    public static function f_encripta($arg_data_s, $arg_max_l, &$arg_data_encriptado_s, &$arg_msj_s)
    {

        $ll_count_caracter = is_long(0);
        $ll_longitud = is_long(0);
        $ll_long_data = is_long(0);
        $ls_car_long = is_string('');
        $ll_random = is_long(0);
        $ls_car_semilla = is_string(0);
        $lb_aviso = is_string('');
        $arg_data_encriptado_s = is_string('');
        $ll_letra = is_long(0);
        $ls_chr_data = is_string('');
        $ls_chr_encriptado = is_string('');
        $ls_car_semilla = '';
        $arg_msj_s = '';
        $arg_cadena_seguridad = array();
        $ll_count_caracter = 0;

        self::f_strlen($arg_data_s, $ll_long_data);

        $ll_letra = 0;

        if ($arg_data_s == '' or is_null($arg_data_s)) {
            $arg_msj_s = 'El dato a encriptar es nulo, favor de verificar';
            return -1;
        }

        if ($ll_long_data <= 0) {
            $arg_msj_s = 'El nùmero de caracteres supera los programados, favor de verificar';
            return -1;
        }

        self::f_crea_char_1al94($ll_long_data, $ls_car_long, $arg_msj_s);

        $ll_random = round((date('s') + 1) / 2);

        self::f_crea_char_1al94($ll_random + 1, $ls_car_semilla, $arg_msj_s);

        if (self::f_crea_cadena_seguridad($ll_random, $lb_aviso, $arg_cadena_seguridad, $arg_msj_s) < 0) {
            return -1;
        }

        if ($lb_aviso == 'false') {
            return -1;
        }

        $arg_data_encriptado_s = $ls_car_semilla . $ls_car_long;

        for ($ll_letra = 1; $ll_letra <= $ll_long_data; $ll_letra++) {
            $ls_chr_data = mb_substr($arg_data_s, $ll_letra - 1, 1, 'UTF-8');
            self::f_get_letra_encriptada($ls_chr_data, $arg_cadena_seguridad, $ls_chr_encriptado, $arg_msj_s);
            $arg_data_encriptado_s .= $ls_chr_encriptado;
        }

        self::f_strlen($arg_data_encriptado_s, $ll_longitud);

        if ($ll_longitud < $arg_max_l) {
            for ($ll_letra = $ll_longitud + 1; $ll_letra <= $arg_max_l; $ll_letra++) {

                if (self::f_crea_char_1al94(rand(0, 20), $ls_chr_encriptado, $arg_msj_s) < 0) {
                    return -1;
                } else {
                    $arg_data_encriptado_s .= $ls_chr_encriptado;
                }
            }
        }

        self::f_valida_caracteres($arg_data_encriptado_s, $ll_count_caracter);

        if ($ll_count_caracter == 1) {
            self::f_encripta($arg_data_s, $arg_max_l, $arg_data_encriptado_s, $arg_msj_s);
        } else {
            $arg_msj_s = 'Se encripto correctamente';
            return 1;
        }
    }

    public static function f_strlen($arg_cadena_s, &$arg_longitud_l)
    {

        $ll_index = is_long(0);
        $ll_count = is_long(0);
        $ll_count = 0;

        for ($ll_index = 0; $ll_index <= strlen($arg_cadena_s); $ll_index++) {
            $ls_caracter = mb_substr($arg_cadena_s, $ll_index, 1, 'UTF-8');

            if ($ls_caracter == 'á' or $ls_caracter == 'é' or $ls_caracter == 'í' or $ls_caracter == 'ó' or $ls_caracter == 'ú' or $ls_caracter == 'Á' or $ls_caracter == 'É' or $ls_caracter == 'Í' or $ls_caracter == 'Ó' or $ls_caracter == 'Ú' or $ls_caracter == 'ñ' or $ls_caracter == 'Ñ') {
                $ll_count++;
            }
        }

        if ($ll_count > 0) {
            $arg_longitud_l = strlen($arg_cadena_s) - $ll_count;
            return 1;
        } else {
            $arg_longitud_l = strlen($arg_cadena_s);
            return 1;
        }
    }

    public static function f_crea_char_1al94($arg_numero_l, &$arg_caracter_s, &$arg_msj_s)
    {

        $gs_cadena_1_94_base_array = array('_', '.', ':', 'x', 'y', 'z', '!', '#', '$', '{', '[', '^', '1', '2', '3', '(', ')', '=', 'D', 'E', 'F', '?', '\\', '¿', 'u', 'v', 'w', 'G', 'H', 'I', '%', '&', '/', '4', '5', '6', 'r', 's', 't', '}', ']', '-', 'J', 'K', 'L', ';', '<', 'o', 'p', 'q', '7', '8', '9', 'M', 'N', 'Ñ', '°', '¬', 'g', 'h', 'i', 'A', 'B', 'C', 'O', 'P', 'Q', '¡', '+', '*', 'd', 'e', 'f', 'R', 'S', 'T', 'm', 'n', 'ñ', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'j', 'k', 'l', '0', '>', ',');



        $ll_count = is_long(0);
        $ls_cadena_base = array();

        $ll_count = 0;
        $ls_cadena_base = '';
        $ls_cadena_base = $gs_cadena_1_94_base_array;
        $ll_count = count($ls_cadena_base);

        if ($arg_numero_l <= 0 or $arg_numero_l > $ll_count) {
            $arg_caracter_s = '';
        } else {
            $arg_caracter_s = $ls_cadena_base[$arg_numero_l - 1];
        }
        $arg_msj_s = 'Caracter correcto';
        return 1;
    }

    public static function f_crea_cadena_seguridad($arg_posicion_l, &$arg_valida_s, &$arg_cadena_seguridad, &$arg_msj_s)
    {

        $gs_cadena_data_base_array = array('F', '?', '\\', '¿', 'y', 'z', '!', '#', '1', '2', '3', '(', 'u', 'v', 'w', 'G', ')', '=', 'D', 'E', 'H', 'I', '%', '&', 'L', ';', '<', 'o', '/', '4', '5', '6', 'p', 'q', '7', '8', 'r', 's', 't', '}', '_', '.', ':', 'x', '9', 'M', 'N', 'Ñ', 'i', 'A', 'B', 'C', '+', '*', 'd', 'e', '$', '{', '[', '^', 'm', 'n', 'ñ', 'U', 'Z', 'a', 'b', 'c', 'j', 'k', 'l', '0', 'V', 'W', 'X', 'Y', 'f', 'R', 'S', 'T', 'O', 'P', 'Q', '¡', '°', '¬', 'g', 'h', ']', '-', 'J', 'K', '>', ',', ' ');
        $arg_valida_s = 'false';
        $ls_cadena_base = array();
        $ls_cadena_nueva = array();
        $ll_incia_en = is_long(0);
        $ll_ele_arr = is_long(0);
        $ll_ele = is_long(0);
        $ll_long_cadena_base = is_long(0);
        $ls_cadena_base = $gs_cadena_data_base_array;
        $ll_long_cadena_base = count($ls_cadena_base);
        $ll_incia_en = $arg_posicion_l;
        $ll_ele_arr = 0;
        $ll_ele = 0;

        for ($ll_ele = $ll_incia_en; $ll_ele < $ll_long_cadena_base; $ll_ele++) {
            $ll_ele_arr++;

            if ($ll_ele_arr > $ll_long_cadena_base) {
                $arg_valida_s = 'false';
                $arg_msj_s = 'Error al crear la cadena de seguridad';
                return -1;
            }

            if ($ls_cadena_base[$ll_ele] == 'NULL' or is_null($ls_cadena_base[$ll_ele])) {
                $ls_cadena_nueva[$ll_ele_arr] = ' ';
            } else {
                $ls_cadena_nueva[$ll_ele_arr] = $ls_cadena_base[$ll_ele];
            }
        }

        for ($ll_ele = 0; $ll_ele < $ll_incia_en; $ll_ele++) {
            $ll_ele_arr++;

            if ($ls_cadena_base[$ll_ele] == 'NULL' or is_null($ls_cadena_base[$ll_ele])) {
                $ls_cadena_nueva[$ll_ele_arr] = ' ';
            } else {
                $ls_cadena_nueva[$ll_ele_arr] = $ls_cadena_base[$ll_ele];
            }
        }

        if (count($ls_cadena_nueva) != $ll_long_cadena_base) {
            $ls_cadena_nueva = '';
            $arg_valida_s = 'false';
            $arg_msj_s = 'Error al al crear la cadena de seguridad';
            return -1;
        } else {
            $arg_cadena_seguridad = $ls_cadena_nueva;
            $arg_valida_s = 'true';
            $arg_msj_s = 'Cadena de seguridad correcta';
            return 1;
        }
    }

    public static function f_get_letra_encriptada($arg_letra_s, $arg_cadena_seguridad, &$arg_chr_s, &$arg_msj_s)
    {

        $gs_cadena_data_base_array = array('F', '?', '\\', '¿', 'y', 'z', '!', '#', '1', '2', '3', '(', 'u', 'v', 'w', 'G', ')', '=', 'D', 'E', 'H', 'I', '%', '&', 'L', ';', '<', 'o', '/', '4', '5', '6', 'p', 'q', '7', '8', 'r', 's', 't', '}', '_', '.', ':', 'x', '9', 'M', 'N', 'Ñ', 'i', 'A', 'B', 'C', '+', '*', 'd', 'e', '$', '{', '[', '^', 'm', 'n', 'ñ', 'U', 'Z', 'a', 'b', 'c', 'j', 'k', 'l', '0', 'V', 'W', 'X', 'Y', 'f', 'R', 'S', 'T', 'O', 'P', 'Q', '¡', '°', '¬', 'g', 'h', ']', '-', 'J', 'K', '>', ',', ' ');
        $ll_ele_original = is_long(0);
        $ll_ele_encontrado = is_long(0);
        $ls_caracter_s = is_string('');
        $ll_ele_original = 0;
        $ll_ele_encontrado = -1;
        $ls_caracter_s = '';

        foreach ($gs_cadena_data_base_array as $ls_caracter_s) {
            $ll_ele_original++;
            if ($arg_letra_s == $ls_caracter_s) {
                $ll_ele_encontrado = $ll_ele_original - 1;
                break;
            }
        }

        if ($ll_ele_encontrado == -1) {
            $arg_chr_s = $arg_letra_s;
        } else {
            $arg_chr_s = $arg_cadena_seguridad[$ll_ele_original];
        }
        return 1;
    }

    public static function f_valida_caracteres($arg_cadena_s, &$arg_indica_l)
    {

        $arg_indica_l = is_long(0);
        $ll_index = is_long(0);
        $ll_strlen = is_long(0);
        $ls_caracter = is_string('');
        $arg_indica_l = 0;
        $ls_caracter = '';
        $ls_combinacion = '';

        self::f_strlen($arg_cadena_s, $ll_strlen);

        for ($ll_index = 0; $ll_index <= $ll_strlen; $ll_index++) {
            $ls_caracter = mb_substr($arg_cadena_s, $ll_index, 1, 'UTF-8');
            $ls_combinacion .= $ls_caracter;

            if ($ll_index == 1) {
                if ($ls_combinacion == 'x:') {
                    $arg_indica_l = 1;
                    break;
                } else if (mb_substr($ls_combinacion, 0, 1, 'UTF-8') == 'Â¿') {
                    $arg_indica_l = 1;
                    break;
                }
            }

            if ($ls_caracter == '<' or $ls_caracter == '>' or $ls_caracter == '&') {
                $arg_indica_l = 1;
                break;
            }
        }
        return 1;
    }

    public static function f_getclientes_direccion(){
        $respuesta = DB::select('SELECT
        clientes.estatus,
        clientes.id_cliente,
        clientes.created_at,
        clientes.deleted_at,
        clientes.con_dias,
        clientes.rfc,
        clientes.razon_social,
        directorio.nombre1,
        directorio.tipo_contacto1,
        directorio.correo1,
        directorio.nombre2,
        directorio.tipo_contacto2,
        directorio.correo2,
        directorio.nombre3,
        directorio.tipo_contacto3,
        directorio.correo3,
        directorio.telefono1,
        directorio.telefono2,
        directorio.telefono3,
        clientes.domicilio
        FROM
        horus.clientes INNER JOIN horus.directorio ON
        clientes.id_cliente =  directorio.id_cliente;');

        foreach($respuesta as $res){
            $res->rfc = $res->rfc != null && isset($res->rfc) ? decrypt(base64_decode($res->rfc)) : '';
            $res->correo1 = $res->correo1 != null && isset($res->correo1) ? decrypt(base64_decode($res->correo1)) : '';
            $res->correo2 = $res->correo2 != null && isset($res->correo2) ?  decrypt(base64_decode($res->correo2)) : '';
            $res->correo3 = $res->correo3 != null && isset($res->correo3) ? decrypt(base64_decode($res->correo3)) : '' ;
            $res->telefono1 = $res->telefono1 != null && isset($res->telefono1) ? decrypt(base64_decode($res->telefono1)) : '';
            $res->telefono2 = $res->telefono2 != null && isset($res->telefono2) ? decrypt(base64_decode($res->telefono2)) : '';
            $res->telefono3 = $res->telefono3 != null && isset($res->telefono3) ? decrypt(base64_decode($res->telefono3)): '';
            $res->domicilio = $res->domicilio != null && isset($res->domicilio) ? decrypt(base64_decode($res->domicilio)) : '';
        }

        //var_dump($respuesta); die();

        return $respuesta;

    }

    public static function f_updateDirectorio($info, $conexion){
        self::setBaseConnection($conexion, 'horus');

        if(!empty($info) && isset($info)){
            foreach($info as $datos){
                if($datos->fecha_baja != ''){
                    DB::insert("INSERT INTO horus.clientes (id, id_cliente, rfc, razon_social, domicilio, ambiente, created_at, updated_at, deleted_at, estatus, con_dias) VALUES (NULL, '$datos->id_cliente', NULL, '$datos->mail', NULL, NULL, '$datos->fecha_alta', NULL, '$datos->fecha_baja', $datos->status, 0 )");
                }else{
                    DB::insert("INSERT INTO horus.clientes (id, id_cliente, rfc, razon_social, domicilio, ambiente, created_at, updated_at, estatus, con_dias) VALUES (NULL, '$datos->id_cliente', NULL, '$datos->mail', NULL, NULL, '$datos->fecha_alta', NULL, $datos->status, 0 )");
                }

                DB::insert("INSERT INTO horus.directorio (id_cliente) VALUES ('$datos->id_cliente')");

            }
            Ambientes::f_obtenerInfoCliente($conexion);
        }
    }

    public static function f_obtenerInfoCliente($conexionH){
        $serverG = Server::where('servers.producto', 'girha')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
        $serverGC = Server::where('servers.producto', 'girha')->where('servers_conexiones.centraliza', 0)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->get();

        Server::decodeServer($serverG,2);
        Ambientes::setBaseConnection($serverG, $serverG->base_central);
        $id_clientes = DB::select('SELECT id_cliente FROM obelsys.clientes WHERE status = 1');
        $contador = 0;
        foreach($id_clientes as $id_cliente){
            Ambientes::setBaseConnection($serverG, $serverG->base_central);
            $ips = DB::select('call sp_obtener_ips_clientes(' . $id_cliente->id_cliente . ')');
            $ips = json_decode(json_encode($ips[0]), true);

            if($contador == 0){
                $servermysqlDB = Lectores::getServerMysqlBD($serverGC, $ips['@ip_mysql_descrip'], 2); //Optiene DB, se usa puede usar para Postgre o MySQL
            }

            Ambientes::setBaseConnection($servermysqlDB, 'cliente_'.$id_cliente->id_cliente);
            try{
                $infoContacto = DB::select("SELECT rfc FROM cliente_$id_cliente->id_cliente.empresas");

                foreach($infoContacto as $data){
                    self::setBaseConnection($conexionH, 'horus');
                    DB::update("UPDATE horus.clientes SET rfc = '".base64_encode(encrypt($data->rfc))."' WHERE id_cliente = $id_cliente->id_cliente");
                    var_dump($data->id_empresa);
                }

            }catch (Exception $e) {
                var_dump("Excepción capturada: ".$e->getMessage());
            }


            $contador .= +1;
        }die();


        return $servermysqlDB;
    }

    public static function f_obtenerInfoForDirectorio(){
        $serverGC = Server::where('servers.producto', 'girha')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();

        if(isset($serverGC)){
            Server::decodeServer($serverGC,2);
            self::setBaseConnection($serverGC, $serverGC->base_central);

            $c_girha = DB::select('SELECT c.id_cliente, c.mail, c.fecha_alta, c.fecha_baja, c.status FROM obelsys.clientes as c');
        }

        return $c_girha;
    }

}

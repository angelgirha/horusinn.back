<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class MailAux extends Model
{
    use HasFactory;
    use Notifiable, HasApiTokens, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombreFrom', 'emailFrom','nombreTo','email','decline','accept',
        'activation', 'emailAccount', 'asunto', 'linea1', 'linea2','acceptText',
        'declineText','actionUrl1', 'actionUrl2', 'acceptA','acceptUText','acceptU','acceptAText'


    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'decline','accept','activation'
    ];

}

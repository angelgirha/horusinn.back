<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use Notifiable, HasApiTokens, SoftDeletes;

    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'nombre', 'email','password','login_usuario','public_key','private_key','status', 'activation_token',


    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
         'remember_token','password','activation_token','private_key','public_key'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public static function getIdUser($email, $status=1)
    {
        return $users = User::selectRaw('id, status')
        ->where('email', $email)
        ->where('status', $status)
        ->whereRaw('deleted_at is null')
        ->get();

    }

    public static function getUsers( $status=1)
    {
        return $users = User::selectRaw('id')
            ->where('status', $status)
            ->whereRaw('deleted_at is null')
            ->get();

    }
    public static function deleteUser($id, $email ){
        
    }
}

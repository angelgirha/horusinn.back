<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BitacoraG extends Model
{
    use HasFactory;

    protected $fillable = [
        'id', 'id_cliente', 'estatus', 'problema', 'observacion', 'comentario_cliente'

    ];

    protected $table = 'clientes_bitacora';

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'registro', 'actualizo', 'created_at', 'updated_at'
    ];

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lectores_cron_horas extends Model
{
    use HasFactory;
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $table = 'lectores_cron_horas';

    protected $primaryKey = 'id';

    protected $fillable = [
        'sentencia','hora','id_lectores_cron'
    ];

    public static function getCronHoras($id)
    {
            $conn = Lectores_cron_horas::where('id_lectores_cron', $id)->get();

        return $conn;
    }

    public static function saveCronHoras($conexiones_datos, $id_server)
    {
        $fallidos = 0;
        $fallaron = "";

        foreach ($conexiones_datos['servers'] as $datos) {
            $validador = self::saveCronHora($datos, $id_server);
            if (!$validador) {
                $fallidos++;
                $fallaron .= " " . $datos['tipo_conexion'];
            }
        }

        return [$fallidos, $fallaron];
    }

    public static function saveCronHora($conn, $id_server)
    {
        $conexion = new Lectores_cron_horas();
        $conexion->sentencia = $conn['sentencia'];
        $conexion->hora = $conn['hora'];
        $conexion->id_lectores_cron = $id_server;
        return $conexion->save();
    }

    public static function updateCronHora($conn, $id_server)
    {
        $conexion = Lectores_cron_horas::where("id", $conn['id'])->where("id_lectores_cron",$id_server)->first();
        $conexion->sentencia = $conn['sentencia'];
        $conexion->hora = $conn['hora'];
        return $conexion->save();
    }

    public static function deleteCronHora($id_conn,$id_server)
    {
        return Lectores_cron_horas::where("id", $id_conn)->where("id_lectores_cron", $id_server)->delete();

    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use stdClass;

use function Symfony\Component\String\s;
use function Symfony\Component\Translation\t;


class Lectores extends Model
{
    use HasFactory;

    protected $hidden = [
        'created_at', 'updated_at'
    ];

    protected $table = 'lectores_info';

    protected $primaryKey = 'id';


    public static function saveLector($lect)
    {
        $lecOrm = new Lectores();
        $lecOrm->no_serie = $lect['no_serie'];
        $lecOrm->nombre = $lect['nombre'];
        $lecOrm->estatus = $lect['estatus'];
        $lecOrm->marcajes_hoy = $lect['marcajes_hoy'];
        $lecOrm->ultima_conexion = $lect['ultima_conexion'];
        $lecOrm->modelo = $lect['modelo'];
        $lecOrm->id_cliente = $lect['id_cliente'];
        return $lecOrm->save();
    }

    public static function deleteLector($id_inc)
    {
        return Clientes::where("id_inc", $id_inc)->delete();

    }

    public static function f_getClientes(){

        $clientes=Clientes::orderBy('id', 'ASC')->get();

        if(count($clientes) > 0){
            if(count(LectoresCron::all())){
                $fecha=LectoresCron::all()[0]['ultima_actualizacion'];
                foreach ($clientes as $cliente){
                    Clientes::decodeCliente($cliente);
                    $cliente['lectores']=Lectores::where("id_cliente",$cliente['id_inc'])->get();
                    $cliente['ultima_actualizacion']=$fecha;
                    $cliente['lectores_linea']=Lectores::where("id_cliente",$cliente['id_inc'])->where('estatus', 1)->where('marcajes_hoy', 0)->count();
                    $cliente['lectores_linea_marcaje']=Lectores::where("id_cliente",$cliente['id_inc'])->where('estatus', 1)->where('marcajes_hoy', '>', 0)->count();
                    $cliente['lectores_fuera_linea']=Lectores::where("id_cliente",$cliente['id_inc'])->where('estatus', 0)->where('marcajes_hoy', 0)->count();
                    $cliente['lectores_fuera_linea_marcaje']=Lectores::where("id_cliente",$cliente['id_inc'])->where('estatus', 0)->where('marcajes_hoy', '>', 0)->count();
                    $cliente['total_lectores'] = $cliente['lectores_linea']+$cliente['lectores_linea_marcaje']+$cliente['lectores_fuera_linea']+$cliente['lectores_fuera_linea_marcaje'];
                }
            }else{
                $clientes = 0;
            }
        }else{
            $clientes = 0;
        }

        return $clientes;
    }


    public static function f_ejecutaGetClientes()
    {
        //f_ejecutaGetClientes

        $clientes = array();
        $serverG = Server::where('servers.producto', 'girha')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
        $serverMA = Server::where('servers.producto', 'mi_asistencia')->where('servers_conexiones.centraliza', 1)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->first();
        $serverMAE = Server::where('servers.producto', 'mi_asistencia')->where('servers_conexiones.centraliza', 0)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->get();

        Ambientes::getBaseConnection($defaultDb , "mysql");

        if(isset($serverG)){
            Server::decodeServer($serverG,2);
            Ambientes::setBaseConnection($serverG, $serverG->base_central);
            $serverActivos = DB::select('SELECT id_cliente FROM obelsys.clientes WHERE status=1');
            $query = 'select c.id_cliente, descripcion_emp from clientes c';
            $query .= ' join configuracion_inicial ci on c.id_cliente = ci.id_cliente';
            $c_girha = DB::select($query);
            foreach ($c_girha as $cliente) {
                $ips = DB::select('call sp_obtener_ips_clientes(' . $cliente->id_cliente . ')');
                $ips = json_decode(json_encode($ips[0]), true);
                array_push($clientes, [
                    'id' => $cliente->id_cliente,
                    'nombre_db' =>"cliente_".$cliente->id_cliente,
                    'nombre' => $cliente->descripcion_emp,
                    'producto' => 'girha',
                    'ip_psql' => $ips['@ip_postgresql_descrip'],
                    'lectores'=> array(),

                ]);
            }
        }

        if(isset($serverMA)){
            $gotPostgreSql=false;
            foreach ($serverMAE as $server) {
                Server::decodeServer($server);
                if ($server->tipo_conexion == "PostgreSql") {
                    $serverMAE = $server;
                    $gotPostgreSql=true;
                    break;
                }
            }
            if($gotPostgreSql){
                Server::decodeServer($serverMA,2);
                Ambientes::setBaseConnection($serverMA, $serverMA->base_central);
                $c_miaasistencia_500 = DB::select('select ID_INDEX, NOMBRE from EMPRESAS where ID_INDEX between 500 and 599');
                foreach ($c_miaasistencia_500 as $cliente) {
                    array_push($clientes, [
                        'id' => $cliente->ID_INDEX,
                        'nombre_db' =>"cliente_".$cliente->ID_INDEX,
                        'nombre' => $cliente->NOMBRE,
                        'producto' => 'mi_asistencia',
                        'ip_psql' => $serverMAE->ip_local,
                        'lectores'=> array(),
                    ]);
                }

                $c_miaasistencia_700 = DB::select('select ID_INDEX, NOMBRE from EMPRESAS where ID_INDEX between 700 and 799');
                foreach ($c_miaasistencia_700 as $cliente) {
                    array_push($clientes, [
                        'id' => $cliente->ID_INDEX,
                        'nombre_db' =>"cliente_".$cliente->ID_INDEX,
                        'nombre' => $cliente->NOMBRE,
                        'producto' => 'zk',
                        'ip_psql' => $serverMAE->ip_local,
                        'lectores'=> array(),
                    ]);
                }
                Ambientes::setBaseConnection($serverMA, 'cloudclo_CloudClock');
                $c_cloudclock = DB::select('select ID_INDEX, NOMBRE from EMPRESAS where ID_INDEX >=100');
                foreach ($c_cloudclock as $cliente) {
                    array_push($clientes, [
                        'id' => $cliente->ID_INDEX,
                        'nombre_db' =>"cloudclo_CloudClock_".$cliente->ID_INDEX,
                        'nombre' => $cliente->NOMBRE,
                        'producto' => 'cloudclock',
                        'ip_psql' => $serverMAE->ip_local,
                        'lectores'=> array(),
                    ]);
                }



            }


        }
        usort($clientes, function($a, $b) {return strcmp($a["ip_psql"], $b["ip_psql"]);});
        Ambientes::setBaseConnection($defaultDb,$defaultDb->bd_name,"mysql");

        self::f_getLectoresByIp($clientes, $defaultDb, $serverActivos);
        if(count(LectoresCron::all()) <= 0){
            LectoresCron::query('INSERT INTO horus.lectores_cron values (1, "Horus monitor", 1, now(), now(), now())');
        }
        $lectoresCron=LectoresCron::find(LectoresCron::max('id'));
        if($lectoresCron == null){
            DB::insert('INSERT INTO horus.lectores_cron values (1, "Horus monitor", 1, now(), now(), now())');
        }
        $lectoresCron->ultima_actualizacion=Db::select("select date_sub(now(), interval 6 hour) as fecha")[0]->fecha;
        $lectoresCron->save();
        //return $clientes;
    }


    public static function f_getLectoresByIp(&$clientes,$defaultDb, $ids)
    {
        $producto="";
        $ip_psql="";
        $clientByIp="";
        $nombre_bd="";
        $resultados = array();
        $iter =1;
        $total=count($clientes);
        Clientes::getQuery()->delete();
        Lectores::getQuery()->delete();
        foreach($clientes as &$cliente){
            Ambientes::setBaseConnection($defaultDb,$defaultDb->bd_name,"mysql");
            Clientes::saveCliente($cliente);
            if($iter==1){
                switch ($cliente["producto"]) {
                    case 'girha':
                        $nombre_bd = 'cliente_';
                        break;
                    case 'cloudclock':
                        $nombre_bd = 'cloudclo_CloudClock_' ;
                        break;
                    default:
                        $nombre_bd = 'miasiste_MiAsistencia_';
                        break;

                }
            }

            if($cliente["producto"]!=$producto or $cliente["ip_psql"]!=$ip_psql or $iter==$total){


                if(strlen($clientByIp)>0){

                    Ambientes::setBaseConnection($defaultDb,$defaultDb->bd_name,"mysql");
                    $productoax=$producto;
                    if($producto=="cloudclock" or $producto=="zk"){
                        $productoax="mi_asistencia";
                    }

                    $serversPSQL = Server::where('servers.producto', $productoax)->where('servers.centraliza', 0)->join('servers_conexiones', 'servers.id', '=', 'servers_conexiones.server_id')->get();

                    $serverpsql = self::getServer($serversPSQL, $ip_psql);
                    $producto=$cliente["producto"];

                    switch ($producto) {
                        case 'girha':
                            $nombre_bd = 'cliente_';
                            break;
                        case 'cloudclock':
                            $nombre_bd = 'cloudclo_CloudClock_' ;
                            break;
                        default:
                            $nombre_bd = 'miasiste_MiAsistencia_';
                            break;
                    }
                    if($iter==$total){
                        $clientByIp.= "'".$nombre_bd.$cliente['id']."',";
                    }
                    $clientByIp= substr($clientByIp, 0, -1);

                    if ($ip_psql != null and $serverpsql != null) {
                            Ambientes::setBaseConnection($serverpsql, "engine", 'pgsql');
                            $query = "select ie.nombre_db,ide.id, iclock_dispositivos.nombre, numero_de_serie as no_serie, modelo, to_char(ultima_conexion at time zone 'UTC+6', 'YYYY-MM-DD HH24:MI:SS') as ultima_conexion, ";
                            $query .= " (CASE WHEN (iclock_dispositivos.ultima_conexion < (now() - interval '3 minute')) THEN 0 else 1 end) as estatus, ";
                            $query .= " ( select count(1) from iclock_marcajes where iclock_marcajes.dispositivo_id = iclock_dispositivos.id and cast(registro as date ) = cast(now() as date) ) as marcajes_hoy";
                            $query .= " from iclock_dispositivos inner join iclock_dispositivos_empresas ide on iclock_dispositivos.id = ide.dispositivos_id ";
                            $query .= " inner join iclock_empresas as ie on ide.empresas_id = ie.id ";
                            $query .= " where ie.nombre_db in (" . $clientByIp . ") ";
                            $query .= " order by nombre_db ASC, estatus ASC, marcajes_hoy ASC; ";

                            $resultados=array_merge($resultados, DB::select($query));

                            if($iter==$total){
                                $clientByIp="";
                            }else{
                                $clientByIp= "'".$nombre_bd.$cliente["id"]."',";
                            }
                    } else {
                        if($iter==$total){
                            $clientByIp="";
                        }else{
                            $clientByIp= "'".$nombre_bd.$cliente["id"]."',";
                        }

                    }
                }
                $producto=$cliente["producto"];
                switch ($producto) {
                    case 'girha':
                        $nombre_bd = 'cliente_';
                        break;
                    case 'cloudclock':
                        $nombre_bd = 'cloudclo_CloudClock_' ;
                        break;
                    default:
                        $nombre_bd = 'miasiste_MiAsistencia_';
                        break;

                }
                $ip_psql=$cliente["ip_psql"];
            }else{
                $clientByIp.= "'".$nombre_bd.$cliente["id"]."',";
            }

            $iter++;
        }
        $producto="";
        $nombre_bd="";
        foreach($clientes as &$cliente){
            $cliente["lectores"]=array();
            if($cliente["producto"]!=$producto ){
                $producto=$cliente["producto"];
                switch ($producto) {
                    case 'girha':
                        $nombre_bd = 'cliente_';
                        break;
                    case 'cloudclock':
                        $nombre_bd = 'cloudclo_CloudClock_';
                        break;
                    default:
                        $nombre_bd = 'miasiste_MiAsistencia_';
                        break;
                }
            }
            $nombre_basefull=$nombre_bd.$cliente["id"];

            foreach ($resultados  as $key => $resultado){

                if(gettype($resultado)=="object" && $nombre_basefull==$resultado->nombre_db){

                    $newLector=[
                        'id' => $resultado->id,
                        'nombre' => $resultado->nombre,
                        'no_serie' => $resultado->no_serie,
                        'modelo' => $resultado->modelo,
                        'ultima_conexion' => $resultado->ultima_conexion,
                        'estatus' => $resultado->estatus,
                        'marcajes_hoy' => $resultado->marcajes_hoy,
                        'id_cliente' => $cliente['id_inc']

                    ];
                   array_push($cliente["lectores"],$newLector );
                   Ambientes::setBaseConnection($defaultDb,$defaultDb->bd_name,"mysql");
                   Lectores::saveLector($newLector);

                    unset($resultados[$key]);
                }
            }
        }
    }

    public static function f_getLectoresByCliente($id, $ip_psql, $producto)
    {
        $serversPSQL = Server::where('producto', $producto)->where('servers.centraliza', 0)->get();
        $serverpsql = self::getServer($serversPSQL, $ip_psql);
        switch ($producto) {
            case 'girha':
                $nombre_bd = 'cliente_' . $id;
                break;
            case 'cloudclock':
                $nombre_bd = 'cloudclo_CloudClock_' . $id;
                break;
            default:
                $nombre_bd = 'miasiste_MiAsistencia_' . $id;
                break;
        }
        if ($ip_psql != null) {
            if ($serverpsql != null) {
                Ambientes::setBaseConnection($serverpsql, "engine", 'pgsql');
                $query = "select ide.id, nombre, numero_de_serie as no_serie, modelo, to_char(ultima_conexion at time zone 'UTC+5', 'YYYY-MM-DD HH24:MI:SS') as ultima_conexion,";
                $query .= " (CASE WHEN (iclock_dispositivos.ultima_conexion < (now() - interval '3 minute')) THEN 0 else 1 end) as estatus,";
                $query .= " ( select count(1) from iclock_marcajes where iclock_marcajes.dispositivo_id = iclock_dispositivos.id and cast(registro as date ) = cast(now() as date) ) as marcajes_hoy";
                $query .= " from iclock_dispositivos inner join iclock_dispositivos_empresas ide on iclock_dispositivos.id = ide.dispositivos_id";
                $query .= " where ide.empresas_id in (select id from iclock_empresas where nombre_db = '" . $nombre_bd . "')";
                $query .= " order by estatus ASC, marcajes_hoy ASC;";
                $resultado = DB::select($query);
                return $resultado;
            } else {
                $lectoresF = array();
                return $lectoresF;
            }
        } else {
            $lectoresF = array();
            return $lectoresF;
        }
    }

    public static function getServer($servers, $ip,$tipo_conexion="PostgreSql")
    {
        $server_P = null;
        foreach ($servers as $server) {
            Server::decodeServer($server);
            if ($server->tipo_conexion == $tipo_conexion && $server->ip_local == $ip) {
                $server_P = $server;
            }
        }
        return $server_P;
    }

    public static function getServerMysqlBD($servers, $ip){
        $server_P = null;
        foreach ($servers as $server) {
            Server::decodeServer($server);
            if($server->ip_local == $ip){
                $server_P = $server;
            }
        }
        return $server_P;
    }

    public static function getServerPostegreBD($servers, $ip){
        $server_P = null;
        foreach ($servers as $server) {
            if($server->ip_local == $ip){
                $server_P = $server;
            }
        }
        return $server_P;
    }

    public static function getBitacoraLectorBySerie($request){
        if(!isset($request->op)){
            $resp = DB::table('horus.lectores_bitacora')->select('lectores_bitacora.*', 'registro.nombre as registro_nombre', 'actualizo.nombre as actualizo_nombre')->where('no_serie', $request->no_serie)->join('users AS registro', 'registro.id', '=', 'lectores_bitacora.registro')->leftJoin('users AS actualizo', 'actualizo.id', '=', 'lectores_bitacora.actualizo')->orderBy('id', 'DESC')->get();
        }elseif($request->op == 2){
            $resp = DB::table('horus.clientes_bitacora')->select('clientes_bitacora.*', 'registro.nombre as registro_nombre', 'actualizo.nombre as actualizo_nombre')->where('id_cliente', $request->id)->join('users AS registro', 'registro.id', '=', 'clientes_bitacora.registro')->leftJoin('users AS actualizo', 'actualizo.id', '=', 'clientes_bitacora.actualizo')->orderBy('id', 'DESC')->get();
        }

        return $resp;
    }

    public static function saveBitacoraLectorBySerie($request){

        $resp = DB::table('lectores_clientes')->select('id')->where('lectores_info.no_serie', "$request->no_serie")->join('lectores_info', 'id_cliente', '=', 'id_inc')->get();

        foreach($resp as $id){
            $insertBitacora = DB::insert("INSERT INTO horus.lectores_bitacora (id, id_cliente, lector, no_serie, estatus, problema, observacion, comentario_cliente, registro, created_at) VALUES (NULL, $id->id, '$request->lector', '$request->no_serie', 'pendiente', '$request->problema', '$request->observacion', '$request->comentario_cliente', '$request->registra', now())");
        }

        return $insertBitacora;
    }
}
